unsigned int draw_offset = select(pos_to_offset(pos0, y, width, height),
                                  pos_to_offset(x, pos0, width, height),
                                  k);

#pragma unroll
for (unsigned int i = 0; i <= DISTANCE_INCREMENT; draw_offset += diff, ++i) {
  canvas[draw_offset] = 0;
}
