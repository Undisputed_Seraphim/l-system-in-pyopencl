__global bool* ptr = canvas + select(pos_to_offset(pos0, y, width, height),
                                     pos_to_offset(x, pos0, width, height),
                                     k);

#pragma unroll
for (unsigned int i = 0; i <= DISTANCE_INCREMENT; ptr += diff, ++i) {
  *ptr = 0;
}
