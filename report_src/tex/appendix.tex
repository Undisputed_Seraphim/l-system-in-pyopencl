% -*- coding: utf-8 -*-
\section[Appendices]
        {Appendices
          \hfill\hrefIncludegraphics[height=2ex]{Koch_curve_5_10_0}}
\subsection{L-systems catalog and turtle interpretations}
\label{subsection:catalog}%
A L-system $G$ is a rewriting system defined by a triplet $(V, \omega, P)$
where
\begin{itemize}[itemsep=0pt]
\item
  $V$
  is an \textit{alphabet}, a set of \textit{symbols}.
  It contains variables (symbols associated to a production rule)
  and constants (symbols that will never replaced).
\item
  $\omega$
  is an \textit{axiom}, a starting string of symbols.
\item
  $P$
  is a set of \textit{production rules} that replace each variable by a sequence of symbols.
\end{itemize}


\bigskip
Here the examples presented in the Wikipedia webpage about L-system
\cite{wikipedia_l-system_2018}.


\subsubsection[Dragon curve]
              {Dragon curve (\href{systems/dragon_curve.txt}{dragon\_curve.txt})}
\label{subsubsection:dragonCurve}%
Alphabet: \texttt{XYF+-}\\
Axiom: \texttt{FX}\\
Production rules:
\begin{tabular}[t]{@{}l@{}}
  \texttt{X $\rightarrow$ X+YF+}\\
  \texttt{Y $\rightarrow$ -FX-Y}
\end{tabular}

\forcenewline[-22ex]%
\begin{figure}[H]\flushright
  \hrefIncludegraphics[width=5cm]{dragon_curve_10_8_90}
  \caption{Dragon curve: \nth{10} iteration, initial angle \ang{90}, distance 8}
\end{figure}

The five first iterations:\\[1ex]
\begin{tabular}{@{}|@{\ }l@{\ }||@{\ }r@{\ }|@{\ }l@{\ }|@{}}
  \hline
  $n$ & len. & expansion\\
  \hline
  \hline
  0 & 2 & \text{FX}\\
  1 & 6 & \text{FX+YF+}\\
  2 & 14 & \text{FX+YF++-FX-YF+}\\
  3 & 30 & \text{FX+YF++-FX-YF++-FX+YF+--FX-YF+}\\
  4 & 62 & \text{FX+YF++-FX-YF++-FX+YF+--FX-YF++-FX+YF++-FX-YF+--FX+YF+--FX-YF+}\\
  5 & 126 & \text{FX+YF++-FX-YF++-FX+YF+--FX-YF++-FX+YF++-FX-YF+--FX+YF+--FX-Y}\dots\\
  & & \dots\\
  \hline
\end{tabular}

\medskip
These information were useful to check the correction of the implementation.

\medskip
We see that the first half of the result of each iteration is
the result of the previous iteration.
Obviously it is possible to use this property
\footnote{
  I don't know how prove this property.}
to accelerate the expanse of the axiom $n$ times.
But I don't used it for this project,
even if the dragon curve is used for the benchmarks.
The implementation is general and accepts all context-free and deterministic L-systems.

\bigskip
\label{dragonCurveLength}%
The following formulas give the number of each symbols
and the total length for $n^{\text{th}}$ iteration
\footnote{
  It is easy to prove them by induction,
  like the formulas in Subsection \ref{subsection:upperBounds}.},
$\forall n \in \naturals :$\\[1ex]
\begin{tabular}{@{}|l||l|l||l|@{}}%
  \hline
  symbol & \multicolumn{2}{l||}{recursive formula} & closed formula\\
  \hline
  \hline
  \texttt{X}
  & $\texttt{X}_0 = 1$ & $\texttt{X}_{n+1} = \texttt{X}_n + \texttt{Y}_n$
  & $\texttt{X}_n \stackrel{(n\neq0)}{=} 2^{n- 1}$\\

  \texttt{Y}
  & $\texttt{Y}_0 = 0$ & $\texttt{Y}_{n+1} = \texttt{X}_n + \texttt{Y}_n$
  & $\texttt{Y}_n \stackrel{(n\neq0)}{=} 2^{n- 1}$\\

  \texttt{F}
  & $\texttt{F}_0 = 1$ & $\texttt{F}_{n+1} = \texttt{F}_n + \texttt{X}_n + \texttt{Y}_n$
  & $\texttt{F}_n = 2^n$\\

  \texttt{+}
  & $\texttt{+}_0 = 0$ & $\texttt{+}_{n+1} = \texttt{+}_n + 2\,\texttt{X}_n$
  & $\texttt{+}_n \stackrel{(n\neq0)}{=} 2^n$\\

  \texttt{-}
  & $\texttt{-}_0 = 0$ & $\texttt{-}_{n+1} = \texttt{-}_n + 2\,\texttt{Y}_n$
  & $\texttt{-}_n \stackrel{(n\neq0)}{=} 2^n - 2$\\
  \hline
  \hline
  length
  & $l_0 = 2$ & $l_{n+1} = l_n + 4\,\texttt{X}_n + 4\,\texttt{Y}_n = l_n + 2^{n+2}$
  & $l_n = 2^{n+2} - 2$\\
  \hline
\end{tabular}

\bigskip
If we consider the expansion of a symbol,
the length of its $n^{\text{th}}$ iteration is,
$\forall n \in \naturals :$\\
$l'_0 = 1$\\
$l'_{n+1} = l'_n + 2^{n+2}$\\[1ex]
\fbox{$l'_n = 2^{n+2} - 3$}

\smallskip
Note that for each variable this corresponds to the $(n-1)^{\text{th}}$ iteration
of the corresponding rule.

\bigskip
Turtle interpretation gives geometric meaning to the symbol
by define what does each symbol.

The \textbf{turtle interpretation}
\label{turtleInterpretation}%
(\cite{rozenberg_lindenmayer_1992} p. 194,
\cite{prusinkiewicz_algorithmic_2004} pp. 46 and 209,
\cite{mishra_l-system_2007} p. 39)
corresponding to the common mathematical convention about angles
(positives angles are counterclockwise) is:\\
\begin{tabular}{@{}l@{\ }l@{}}
  \texttt{F}: & draw a line of a predeterminated distance\\
  \texttt{f}: & move of this same distance but without draw\\
  \texttt{+}: & turn left of a predeterminated angle\\
  \texttt{-}: & turn right of the same angle\\
  \texttt{[}: & push the current position and the current angle on the stack\\
  \texttt{]}: & pop the stack and restore the current position and the current orientation\\
\end{tabular}\\
and other symbols are ignored.

\medskip
But each L-system can define its proper turtle interpretation.
Specially the meaning of \texttt{+} and \texttt{-}
is a source of confusions.
\footnote{
  The book \cite{mishra_l-system_2007}
  presents the correct interpretation p. 40,
  but contains a typo p. 29:\\
  \itblockquote{%
    $+$ Turn left by angle $b$. The next state of the turtle is $(x,y,a+b)$.\\
    \phantom{``}$-$ Turn left by angle $b$. The next state of the turtle is $(x,y,a-b)$.}}
My \texttt{Turtle} implementation adopts this orientation
corresponding to the common mathematical convention about angles.

\begin{figure}[H]\center
  \input{tikz/angles.tex}
  \caption{Counterclockwise orientation.}
\end{figure}

\medskip
But the mapping file
\href{systems/turtle/turtle_dragon_curve.txt}
     {\texttt{turtle\_dragon\_curve.txt}}
used inverses the meaning of \texttt{+} and \texttt{-}
corresponding to \cite{wikipedia_l-system_2018}.
\footnote{
  It would have been better to be coherent with previous references,
  but results of experimentation have been computed with this meaning.
  The confusion for me was clarified too late.}

The turtle interpretation for the dragon curve used in this work is:\\
\begin{tabular}{@{}l@{\ }l@{}}
  \texttt{F}: & draw a line\\
  \texttt{+}: & turn right \ang{90}\\
  \texttt{-}: & turn left \ang{90}\\
\end{tabular}\\
and other symbols are ignored.


\subsubsection[\textsc{Cantor} set]
              {\textsc{Cantor} set (\href{systems/Cantor_set.txt}{Cantor\_set.txt})}
Alphabet: \texttt{AB}\\
Axiom: \texttt{A}\\
Production rules:
\begin{tabular}[t]{@{}l@{}}
  \texttt{A $\rightarrow$ ABA}\\
  \texttt{B $\rightarrow$ BBB}
\end{tabular}

\forcenewline[-9ex]%
\begin{figure}[H]\flushright
  \hrefIncludegraphics[width=8cm]{Cantor_set_3_5_0}\\
  \caption{\textsc{Cantor} set: \nth{3} iteration, initial angle \ang{0}, distance 5}
\end{figure}

\medskip
Turtle interpretation for the \textsc{Cantor} set
(\href{systems/turtle/turtle_Cantor_set.txt}{turtle\_Cantor\_set.txt}):\\
\begin{tabular}{@{}l@{\ }l@{}}
  \texttt{A}: & draw a line\\
  \texttt{B}: & move\\
\end{tabular}


\subsubsection[Fractal binary tree]
              {Fractal binary tree (\href{systems/fractal_binary_tree.txt}{fractal\_binary\_tree.txt})}
Alphabet: \texttt{01[]}\\
Axiom: \texttt{0}\\
Production rules:
\begin{tabular}[t]{@{}l@{}}
  \texttt{0 $\rightarrow$ 1[0]0}\\
  \texttt{1 $\rightarrow$ 11}
\end{tabular}

\forcenewline[-22ex]%
\begin{figure}[H]\flushright
  \hrefIncludegraphics[width=5cm]{fractal_binary_tree_7_10_90}
  \caption{Fractal binary tree: \nth{7} iteration, initial angle \ang{90}, distance 10}
\end{figure}

\bigskip
$\forall n \in \naturals :$\\[1ex]
\begin{tabular}{@{}|l||l|l||l|@{}}%
  \hline
  symbol & \multicolumn{2}{l||}{recursive formula} & closed formula\\
  \hline
  \hline
  \texttt{0}
  & $\texttt{0}_0 = 1$ & $\texttt{0}_{n+1} = 2\,\texttt{0}_n$
  & $\texttt{0}_n = 2^n$\\

  \texttt{1}
  & $\texttt{1}_0 = 0$ & $\texttt{1}_{n+1} = 2\,\texttt{1}_n + \texttt{0}_n$
  & $\texttt{1}_n = n\,2^{n-1}$\\

  \texttt{[}
  & $\texttt{[}_0 = 0$ & $\texttt{[}_{n+1} = \texttt{[}_n + \texttt{0}_n$
  & $\texttt{[}_n = 2^n - 1$\\

  \texttt{]}
  & $\texttt{]}_0 = 0$ & $\texttt{]}_{n+1} = \texttt{]}_n + \texttt{0}_n$
  & $\texttt{]}_n = 2^n - 1$\\
  \hline
  \hline
  length
  & $l_0 = 1$ & $l_{n+1} = l_n + 4\,\texttt{0}_n + \texttt{1}_n$
  & $l_n = (n + 6) 2^{n-1} - 2$\\
  \hline
\end{tabular}

\bigskip
Turtle interpretation for the fractal binary tree
(\href{systems/turtle/turtle_fractal_binary_tree.txt}{turtle\_fractal\_binary\_tree.txt}):\\
\begin{tabular}{@{}l@{\ }l@{}}
  \texttt{0}: & draw a line (corresponding to the ending of a leaf)\\
  \texttt{1}: & draw a line\\
  \texttt{[}: & push position and angle, then turn left \ang{45}\\
  \texttt{]}: & pop position and angle, then turn right \ang{45}\\
\end{tabular}


\subsubsection[Fractal plant]
              {Fractal plant (\href{systems/fractal_plant.txt}{fractal\_plant.txt})}
Alphabet: \texttt{XF+-[]}\\
Axiom: \texttt{X}\\
Production rules:
\begin{tabular}[t]{@{}l@{}}
  \texttt{X $\rightarrow$ F+[[X]-X]-F[-FX]+X}\\
  \texttt{F $\rightarrow$ FF}
\end{tabular}

\forcenewline[-23ex]%
\begin{figure}[H]\flushright
  \hrefIncludegraphics[width=5cm]{fractal_plant_6_3_65}
  \caption{Fractal plant: \nth{6} iteration, initial angle \ang{65}, distance 6}
\end{figure}

\bigskip
Turtle interpretation for the fractal plant
(\href{systems/turtle/turtle_fractal_plant.txt}{turtle\_fractal\_plant.txt}):\\
the conventional interpretation with an angle of \ang{25}.


\subsubsection[\textsc{Koch} curve]
              {\textsc{Koch} curve (\href{systems/Koch_curve.txt}{Koch\_curve.txt})}
Alphabet: \texttt{F+-}\\
Axiom: \texttt{F}\\
Production rules: \texttt{F $\rightarrow$ F+F-F-F+F}

\forcenewline[-21ex]%
\begin{figure}[H]\flushright
  \hrefIncludegraphics[width=5cm]{Koch_curve_3_10_0}
  \caption{\textsc{Koch} curve: \nth{3} iteration, initial angle \ang{0}, distance 10}
\end{figure}

\medskip
Turtle interpretation for the \textsc{Koch} curve
(\href{systems/turtle/turtle_Koch_curve.txt}{turtle\_Koch\_curve.txt}):\\
the conventional interpretation with an angle of \ang{90}.


\subsubsection[\textsc{Koch} snowflake]
              {\textsc{Koch} snowflake (\href{systems/Koch_snowflake.txt}{Koch\_snowflake.txt})}
Alphabet: \texttt{F+-}\\
Axiom: \texttt{F--F--F}\\
Production rules: \texttt{F $\rightarrow$ F+F--F+F}

\forcenewline[-21ex]%
\begin{figure}[H]\flushright
  \hrefIncludegraphics[width=5cm]{Koch_snowflake_5_10_0}
  \caption{\textsc{Koch} snowflake: \nth{5} iteration, initial angle \ang{0}, distance 10}
\end{figure}

\medskip
Turtle interpretation for the \textsc{Koch} snowflake
(\href{systems/turtle/turtle_Koch_snowflake.txt}{turtle\_Koch\_snowflake.txt}):\\
the conventional interpretation with an angle of \ang{60}.


\subsubsection[\textsc{Sierpinski} arrowhead curve]
              {\textsc{Sierpinski} arrowhead curve (\href{systems/Sierpinski_arrowhead_curve.txt}{Sierpinski\_arrowhead\_curve.txt})}
Alphabet: \texttt{AB+-}\\
Axiom: \texttt{A}\\
Production rules:
\begin{tabular}[t]{@{}l@{}}
  \texttt{A $\rightarrow$ B-A-B}\\
  \texttt{B $\rightarrow$ A+B+A}
\end{tabular}

\forcenewline[-18ex]%
\begin{figure}[H]\flushright
  \hrefIncludegraphics[width=5cm]{Sierpinski_arrowhead_curve_8_1_0}
  \caption{\textsc{Sierpinski} arrowhead curve: \nth{8} iteration, initial angle \ang{0}, distance 1}
\end{figure}

\medskip
Turtle interpretation for the \textsc{Sierpinski} arrowhead curve
(\href{systems/turtle/turtle_Sierpinski_arrowhead_curve.txt}{turtle\_Sierpinski\_arrowhead\_curve.txt}):\\
\begin{tabular}{@{}l@{\ }l@{}}
  \texttt{A}: & draw a line\\
  \texttt{B}: & draw a line\\
  \texttt{+}: & turn left \ang{60}\\
  \texttt{-}: & turn right \ang{60}\\
\end{tabular}


\subsubsection[\textsc{Sierpinski} triangle]
              {\textsc{Sierpinski} triangle (\href{systems/Sierpinski_triangle.txt}{Sierpinski\_triangle.txt})}
Alphabet: \texttt{FG+-}\\
Axiom: \texttt{F-G-G}\\
Production rules:
\begin{tabular}[t]{@{}l@{}}
  \texttt{F $\rightarrow$ F-G+F+G-F}\\
  \texttt{G $\rightarrow$ GG}
\end{tabular}

\forcenewline[-18ex]%
\begin{figure}[H]\flushright
  \hrefIncludegraphics[width=5cm]{Sierpinski_triangle_6_4_90}
  \caption{\textsc{Sierpinski} triangle: \nth{6} iteration, initial angle \ang{90}, distance 4}
\end{figure}

\medskip
Turtle interpretation for the \textsc{Sierpinski} triangle
(\href{systems/turtle/turtle_Sierpinski_triangle.txt}{turtle\_Sierpinski\_triangle.txt}):\\
\begin{tabular}{@{}l@{\ }l@{}}
  \texttt{F}: & draw a line\\
  \texttt{G}: & draw a line\\
  \texttt{+}: & turn left \ang{120}\\
  \texttt{-}: & turn right \ang{120}\\
\end{tabular}


\subsection{Upper bounds on some sizes}
\label{subsection:upperBounds}%
Let, about a string,\\
$s_v \assign$ the number of \textit{variables} of this string $> 0$\\
$s_c \assign$ the number of \textit{constants} of this string $\geq 0$\\
$s_s \assign$ the \textit{size} of this string $> 0$

\medskip
And let, about the production rules,\\
$R_v \assign$ the \textit{maximum} number of \textit{variables} in a rule $> 0$\\
$R_c \assign$ the \textit{maximum} number of \textit{constants} in a rule $\geq 0$\\
$R_s \assign$ the \textit{maximum} \textit{size} of a rule $> 0$

\medskip
We have:\\
$s_v + s_c = s_s$\\
$R_v + R_c \geq R_s$

\medskip
Let, forall $n \in \naturals$,
$S_{v, n}, S_{c, n}$ and $S_{s, n}$
the corresponding \textit{upper bounds}
for the $n^{\text{th}}$ iteration of $s_v, s_c$ and $s_s$,
i.e. the $n^{\text{th}}$ iteration of the expansion of the string.

\medskip
\textbf{Upper bound for the number of variables}.
In one iteration, each constant remains a constant,
and each variable is replaced by a maximum of $R_v$ variables:\\
$S_{v,0} = s_v$\\
$S_{v,n+1} = S_{v,n} R_v
\geq S_{v,n}$\\
Thus $S_{v,n} = s_v R_v^n$
\qquad$\in \bigO(R_v^n)$

This last relation and the following ones
are easily demonstrated by induction.

\medskip
\textbf{Upper bound for the number of constants}.
In one iteration, each constant remains a constant,
and each variable is replaced by a maximum of $R_c$ constant:\\
$S_{c,0} = s_c$\\
$S_{c,n+1} = S_{c,n} + S_{v,n} R_c
\geq S_{c,n}$\\
Thus $S_{c,n}
= s_c + s_v R_c \sum\limits_{i=0}^{n-1} R_v^i
\stackrel{(R_v \neq 1)}{=} s_c + s_v R_c \frac{R_v^n - 1}{R_v - 1}$
\qquad$\in \bigO(R_v^n)$

\medskip
\textbf{Upper bound for the size}.
In one iteration, each constant remains a constant,
and each variable is replaced by a maximum of $R_s$ symbols.
But instead directly consider $S_{c,n}$
is better to consider $S_{s,n} - S_{v,n}$
(because $S_{c,n} \geq S_{s,n} - S_{v,n}$):\\
$S_{s,0} = s_s$\\
$S_{s,n+1} = S_{s,n} - S_{v,n} + S_{v,n} R_s = S_{s,n} + S_{v,n} (R_s - 1)
\geq S_{s,n}$\\[1ex]
Thus
\fbox{$S_{s,n}
  = s_s + s_v (R_s - 1) \sum\limits_{i=0}^{n-1} R_v^i$}
$\stackrel{(R_v \neq 1)}{=} s_s + s_v (R_s - 1) \frac{R_v^n - 1}{R_v - 1}
\leq S_{v,n} + S_{c,n}$\\[1ex]
and $S_{s,n} \in \bigO(R_v^n)$

\bigskip
This upper bound is exact for the dragon curve:\\
$S_{s,n}
= 2 + 1 (5 - 1) \sum\limits_{i=0}^{n-1} 2_v^i
= 2 + 4 (2^n -1)
= 2^{n-2} - 2$\\
that is exactly the size computed p. \pageref{dragonCurveLength}.


\bigskip
Consider now the expansion of the production rules by themselves.
We can consider that the variable transformed by a production rule is an axiom.

Let, forall $n \in \naturals$,
$R_{v, n}, R_{c, n}$ and $R_{s, n}$
the corresponding \textit{upper bounds}
for the $n^{\text{th}}$ iteration of $R_v, R_c$ and $R_s$,
i.e. the $n^{\text{th}}$ iteration of the expansion of the production rules
such that
$R_{v,1} = R_v$, $R_{c,1} = R_c$ and $R_{s,1} = R_s$.

With the same reasoning that before we obtain these relations.

\medskip
\noindent
$R_{v,0} = 1$\\
$R_{v,n+1} = R_{v,n} R_v
\geq R_{v,n}$\\
Thus $R_{v,n} = R_v^n$
\qquad$\in \bigO(R_v^n)$

\medskip
\noindent
$R_{c,0} = 0$\\
$R_{c,n+1} = R_{c,n} + R_{v,n} R_c
\geq R_{c,n}$\\
Thus $R_{c,n}
= R_c \sum\limits_{i=0}^{n-1} R_v^i
\stackrel{(R_v \neq 1)}{=} R_c \frac{R_v^n - 1}{R_v - 1}$
\qquad$\in \bigO(R_v^n)$

\medskip
\noindent
$R_{s,0} = 1$\\
$R_{s,n+1} = R_{s,n} - R_{v,n} + R_{v,n} R_s = R_{s,n} + R_{v,n} (R_s - 1)
\geq R_{s,n}$\\[1ex]
Thus
\fbox{$R_{s,n}
  = 1 + (R_s - 1) \sum\limits_{i=0}^{n-1} R_v^i$}
$\stackrel{(R_v \neq 1)}{=} 1 + (R_s - 1) \frac{R_v^n - 1}{R_v - 1}
\leq R_{v,n} + R_{c,n}$\\[1ex]
and $R_{s,n} \in \bigO(R_v^n)$

\bigskip
This upper bound is also exact for the dragon curve.


\subsection{Some identified bugs with OpenCL and/or \textit{ranger}!}
\label{subsection:bugsOpenCL}%
\forcenewline[-2ex]
\null\hfill
\begin{minipage}{4cm+2pt}\center
  \fboxsep=0pt
  \fbox{\hrefIncludegraphics[width=4cm]{000-005-Unexplained-Phenomena-Software}}\\
  (Rob \textsc{Russell} \cite{russell_000-005_2018})
\end{minipage}


\subsubsection[\texttt{pyopencl.get\_platforms()} with pytest crash on \textit{ranger}]
              {\texttt{pyopencl.get\_platforms()} with pytest \cite{pytest_pytest_nodate} crash on \textit{ranger}}
All my OpenCL unit tests crash on the computer \textit{ranger}.
After a long period of total misunderstanding
I finally identified that the problem wasn't my code
but the mix of PyOpenCL and pytest and \textit{ranger}!

This simple unit test
\href{src/test/test__opencl_getplatforms.py}{\texttt{test\_\_opencl\_getplatforms.py}}
only runs the
\href{https://documen.tician.de/pyopencl/runtime_platform.html?highlight=get_platforms#platform}
     {\texttt{pyopencl.get\_platforms()}}
command.
It works fine on my computer, both when it is ran by pytest and it is ran directly.
But on the computer \textit{ranger}
it simply crashes when it is ran by pytest!
Whereas it works fine when it is ran directly.


\subsubsection{Array\texttt{[]} vs pointer\texttt{*}}
\texttt{canvas} is declared like this by the kernel:
\texttt{\_\_global bool* const canvas}.

\medskip
In C,
\texttt{canvas[draw\_offset]}
is equivalent to
\texttt{*(canvas + draw\_offset)}.

\medskip
Normally it is the same in OpenCL,
and I think that the two following codes are equivalent.

This version with array access works both on my computer and on the computer \textit{ranger}.
\lstinputlisting[language=C]{txt/algo/array.cl}

This version with pointer works fine on my computer but crashes on \textit{ranger}.
\lstinputlisting[language=C]{txt/algo/pointer.cl}

See the complete kernel:
\href{src/l_system/kernel/opt_turtle.cl}{\texttt{opt\_turtle.cl}}.


\subsubsection{\texttt{Broken pipe}}
Several times I have lost the connection with the computer \textit{ranger}
and that had stopped my running processes!
The error message has printed on my console:\\
\texttt{"packet\_write\_wait: Connection to 134.184.43.128 port 22: Broken pipe"}


\subsection{Some tips with OpenCL}
Some tips and knowledge found during the development of this project.
Some obvious, and some more elaborate.

\bigskip
On old GPU the \texttt{printf} is not available.
And it is very annoying during the development.
But the CPU accept more recent version of OpenCL and \texttt{printf}.
So run on CPU can be very useful to identify some problems.
It is not the panacea since the running of the program on CPU
will be probably a different intern behaviour.

\bigskip
The clang compiler can be used to compile kernel, check syntax and have some warnings.\\
\texttt{clang -fsyntax-only -Xclang -finclude-default-header -cl-std=CL1.1 -pedantic -Wall -Wextra kernel.cl}

\bigskip
With a conditional extra parameter transmit to the kernel by the Python code
it is possible to have implemented (limited) C \texttt{assert}:
\hrefShow{src/l\_system/kernel/assert.cl}.
\footnote{
  I finalized this first implementation
  and I made it one library:
  \parbox{11ex}{\hrefIncludegraphics[width=11ex]{assertOpenCL}}
  \texttt{assertOpenCL} \cite{pirson_assertopencl_2018}.}


\subsubsection{Disable the compiler cache of OpenCL}
On Debian GNU/Linux 9 (Stretch)
the NVIDIA OpenCL compiler keeps cache information
in the \texttt{~/.nv/ComputeCache/} directory.

It seems not clear
if the compiler recompiles each time it should,
specially with files included.
During the development phase it may be useful to disable this cache
(it \textit{seems} that you also can delete this directory whenever you wish).

In a console, do:\\
\texttt{\$ export CUDA\_CACHE\_DISABLE=1}


\subsection{Material used during the development}
\label{DellPrecision}%
All the code was developed, tested and optimized on a old Dell Precision T3500:
\begin{itemize}[itemsep=0pt]
\item
  Processor Intel Xeon \textbf{quad} core W3530 \textbf{2,8 GHz}
  (\textbf{8} threads with hyper-threading)
  \cite{intel_intel_nodate}.
  Turbo Boost enabled.
\item
  \begin{tabular}[t]{@{}l@{\ }r@{\ }l}
    Cache L$1$d: & $32$ & Kb\\
    Cache L$1$i: & $32$ & Kb\\
    Cache L$2$: & $256$ & Kb\\
    Cache L$3$: & $8$ & Mb
  \end{tabular}
\item
  RAM: 6 Gb
\item
  See \hrefShow{infos/Dell\_Precision\_T3500/opencl\_infos\_complete.txt}
  for complete information about CPU and GPU by OpenCL.
\item
  GPU: NVIDIA quadro FX 1800 (768 Mio) \cite{nvidia_nvidia_nodate}.
  \begin{itemize}[itemsep=0pt]
  \item
    global mem cache size: 0 o
  \item
    global mem size: 804585472 o = 785728 kio = 767 Mio
  \item
    local mem size: 16384 o = 16 kio
  \item
    max clock frequency: 1375
  \item
    max compute units: 8
  \item
    max constant buffer size: 65536 o = 64 kio
  \item
    max mem alloc size: 201146368 o = 196432 kio = 191 Mio
  \item
    max work group size: 512 o
  \item
    max work item sizes: 512 512 64
  \item
    opencl c version: \textbf{OpenCL C 1.0}
  \end{itemize}
\item
  Operating system: Debian GNU/Linux 9 (Stretch) \textbf{64 bits}.
\item
  Python 2.7.13,
  Python 3.5.3
  and partially (neither NumPy or OpenCL) PyPy 5.6.0 (Python 2.7.12)
\end{itemize}


\input{tex/appendix_results}
