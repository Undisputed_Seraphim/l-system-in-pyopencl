#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Run a simple OpenCL kernel with several parameters
to benchmark copy and inc numbers from a buffer to an other.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 14, 2018
"""

from __future__ import division
from __future__ import print_function

import gc
import sys
import time

import numpy as np
import pyopencl as cl

import l_system.opencl_l_system


if sys.version_info[0] >= 3:
    long = int


def check_opencl(nb_work_group, nb_work_items_by_work_group, device=None):
    """
    Run the simple check() kernel
    to verify the number of work groups and work units
    and make a simple verification that work fine.

    :param nb_work_group: int > 0
    :param nb_work_items_by_work_group: int > 0
    :param device: None or OpenCL device
    """
    assert isinstance(nb_work_group, int), type(nb_work_group)
    assert nb_work_group > 0, nb_work_group

    assert isinstance(nb_work_items_by_work_group, int), type(nb_work_items_by_work_group)
    assert nb_work_items_by_work_group > 0, nb_work_items_by_work_group

    # Host buffers
    h_sizes = np.empty(3).astype(np.uint32)
    h_sizes.fill(42)
    h_results = np.empty(nb_work_group).astype(np.uint32)
    h_results.fill(42)

    # OpenCL kernel
    context = (cl.create_some_context() if device is None
               else cl.Context((device, )))
    kernel = open('l_system/kernel/copy_inc.cl').read()
    program = cl.Program(context, kernel).build()

    # OpenCL context and queue
    queue = cl.CommandQueue(context,
                            properties=cl.command_queue_properties.PROFILING_ENABLE)

    # Device buffers
    mf = cl.mem_flags
    d_sizes = cl.Buffer(context, mf.WRITE_ONLY | mf.COPY_HOST_PTR,
                        hostbuf=h_sizes)
    d_results = cl.Buffer(context, mf.WRITE_ONLY | mf.COPY_HOST_PTR,
                          hostbuf=h_results)

    print('Check {}: {} work groups x {} work items'
          .format(device, nb_work_group, nb_work_items_by_work_group), end='\t')
    sys.stdout.flush()

    # Run
    f = program.check
    f.set_scalar_arg_dtypes([None, None])
    global_size = nb_work_items_by_work_group * nb_work_group
    f(queue, (global_size, ), (nb_work_items_by_work_group, ),
      d_sizes, d_results)

    queue.finish()

    # Result
    cl.enqueue_copy(queue, h_sizes, d_sizes)
    cl.enqueue_copy(queue, h_results, d_results)

    if tuple(h_sizes) != (nb_work_group, global_size, nb_work_items_by_work_group):
        print('\nFAILED', h_sizes)

        sys.exit(1)

    for i in range(nb_work_group):
        if h_results[i] != i:
            print('\nFAILED', i, h_results[i], h_results)

            sys.exit(1)


def copy_inc_opencl(buffer_size,
                    nb_work_group, nb_work_items_by_work_group, device=None):
    """
    Create two buffers of size buffer_size,
    init the first of them and run copy_inc() kernel.
    Finally check if the result is correct.
    """
    assert isinstance(buffer_size, int), type(buffer_size)
    assert buffer_size > 0, buffer_size

    assert isinstance(nb_work_group, int), type(nb_work_group)
    assert nb_work_group > 0, nb_work_group

    assert isinstance(nb_work_items_by_work_group, int), type(nb_work_items_by_work_group)
    assert nb_work_items_by_work_group > 0, nb_work_items_by_work_group

    start = time.time()

    check_opencl(nb_work_group, nb_work_items_by_work_group, device)

    # Host buffers
    h_src = np.empty(buffer_size).astype(np.uint8)
    h_dest = np.empty(buffer_size).astype(np.uint8)

    for i in range(buffer_size):
        h_src[i] = i % 256
    h_dest.fill(42)

    # OpenCL kernel
    context = (cl.create_some_context() if device is None
               else cl.Context((device, )))
    kernel = open('l_system/kernel/copy_inc.cl').read()
    program = cl.Program(context, kernel).build()

    # OpenCL context and queue
    queue = cl.CommandQueue(context,
                            properties=cl.command_queue_properties.PROFILING_ENABLE)

    # Device buffers
    mf = cl.mem_flags
    d_src = cl.Buffer(context, mf.READ_ONLY | mf.COPY_HOST_PTR,
                      hostbuf=h_src)
    d_dest = cl.Buffer(context, mf.WRITE_ONLY | mf.COPY_HOST_PTR,
                       hostbuf=h_dest)

    # Run
    f = program.copy_inc
    f.set_scalar_arg_dtypes([None, np.uint32,
                             None])
    global_size = nb_work_items_by_work_group * nb_work_group

    assert buffer_size % global_size == 0, (buffer_size, global_size)

    event = f(queue, (global_size, ), (nb_work_items_by_work_group, ),
              d_src, buffer_size,
              d_dest)

    queue.finish()

    # Result
    cl.enqueue_copy(queue, h_dest, d_dest)
    opencl_duration_ns = event.profile.end - event.profile.start

    duration_s = time.time() - start

    for i in range(buffer_size):
        if (h_src[i] != i % 256) or (h_dest[i] != (i + 1) % 256):
            print('\nFAILED', i, h_dest[i])
            print(h_dest)

            sys.exit(1)

    return (opencl_duration_ns, duration_s)


def size_units(n):
    """
    Return a string with the size n expressed in several units.

    :param: (int or long) >= 0

    :return: str
    """
    assert isinstance(n, int) or isinstance(n, long), type(n)
    assert n >= 0, n

    seq = ['{} o'.format(n)]
    units = 'kMGT'

    while True:
        n //= 1024
        unit, units = units[0], units[1:]
        if n <= 0:
            break

        seq.append('{} {}io'.format(n, unit))

    return ' = '.join(seq)


def time_units(ns):
    """
    Return a string with the time ns expressed in several units.

    :param: (int or long) >= 0

    :return: str
    """
    assert isinstance(ns, int) or isinstance(ns, long), type(ns)
    assert ns >= 0, ns

    return '{} ns = {:.3f} s'.format(ns, float(ns) / (10**9))


########
# Main #
########
def main():
    """
    Run test with increasing number of work units,
    and the with increasing number of work groups.
    """
    platform_id = 0
    try:
        platform_id = int(sys.argv[1])
    except:
        pass

    device = l_system.opencl_l_system.get_device(platform_id, 0)

    buffer_size = 2**20

    for i in range(10):
        nb_work_group = 1
        nb_work_items_by_work_group = 2**i
        global_size = nb_work_group * nb_work_items_by_work_group

        print(i, buffer_size, nb_work_group, global_size, nb_work_items_by_work_group,
              sep='\t', end='\t')
        sys.stdout.flush()

        time.sleep(1)
        opencl_duration_ns, duration_s \
            = copy_inc_opencl(buffer_size, nb_work_group, nb_work_items_by_work_group, device)

        print(time_units(opencl_duration_ns), '{:.3f} s'.format(duration_s),
              sep='\t')
        gc.collect()

    print()
    for i in range(12):
        nb_work_group = 2**i
        nb_work_items_by_work_group = 512
        global_size = nb_work_group * nb_work_items_by_work_group

        print(i, buffer_size, nb_work_group, global_size, nb_work_items_by_work_group,
              sep='\t', end='\t')
        sys.stdout.flush()

        time.sleep(1)
        opencl_duration_ns, duration_s \
            = copy_inc_opencl(buffer_size, nb_work_group, nb_work_items_by_work_group, device)

        print(time_units(opencl_duration_ns), '{:.3f} s'.format(duration_s),
              sep='\t')
        gc.collect()

if __name__ == '__main__':
    main()
