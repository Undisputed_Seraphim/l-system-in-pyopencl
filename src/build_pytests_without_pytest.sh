#!/bin/sh

> pytests_without_pytest.log

for filename in test/test__*.py
do
    echo ==================== Begin $filename done ==================== >> pytests_without_pytest.log
    python $filename >> pytests_without_pytest.log 2>&1
    echo ==================== $filename done ==================== >> pytests_without_pytest.log
done
