# -*- coding: latin-1 -*-

"""
Helper constants for tests.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 13, 2018
"""

import os.path


# https://en.wikipedia.org/wiki/L-system#Example_1:_Algae
ALGAE = ('A',
         'AB',
         'ABA',
         'ABAAB',
         'ABAABABA',
         'ABAABABAABAAB',
         'ABAABABAABAABABAABABA',
         'ABAABABAABAABABAABABAABAABABAABAAB')

# https://en.wikipedia.org/wiki/L-system#Example_2:_Fractal_(binary)_tree
FRACTAL_BINARY_TREE = ('0',
                       '1[0]0',
                       '11[1[0]0]1[0]0',
                       '1111[11[1[0]0]1[0]0]11[1[0]0]1[0]0')

# https://en.wikipedia.org/wiki/L-system#Example_4:_Koch_curve
KOCH_CURVE = ('F',
              'F+F-F-F+F',
              'F+F-F-F+F+F+F-F-F+F-F+F-F-F+F-F+F-F-F+F+F+F-F-F+F',
              'F+F-F-F+F+F+F-F-F+F-F+F-F-F+F-F+F-F-F+F+F+F-F-F+F+F+F-F-F+F+F+F-F-F+F-F+F-F-F+F-F+F-F-F+F+F+F-F-F+F-F+F-F-F+F+F+F-F-F+F-F+F-F-F+F-F+F-F-F+F+F+F-F-F+F-F+F-F-F+F+F+F-F-F+F-F+F-F-F+F-F+F-F-F+F+F+F-F-F+F+F+F-F-F+F+F+F-F-F+F-F+F-F-F+F-F+F-F-F+F+F+F-F-F+F')

EXAMPLES_DIR = os.path.dirname(__file__) + '/../../systems/'

EXAMPLES = {'algae': ALGAE,
            'fractal binary tree': FRACTAL_BINARY_TREE,
            'Koch curve': KOCH_CURVE}
