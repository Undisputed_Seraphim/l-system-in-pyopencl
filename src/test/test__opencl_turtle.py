#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Tests class of OpenCLTurtle from l_system.opencl_turtle.py.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 18, 2018
"""

from __future__ import print_function

import glob
import os.path
import sys

import numpy as np

try:
    import pytest
except ImportError:
    print('! pytest module not installed', file=sys.stderr)

import test
import test__turtle

import l_system.simple_l_system as simple_l_system
import l_system.fast_l_system as fast_l_system
import l_system.opencl_l_system as opencl_l_system
import l_system.turtle as turtle

import l_system.opencl_turtle as opencl_turtle


OPENCL_DEBUG = False


def test__abop_figure_1_5_bitmap():
    """
    Process the example from "The Algorithmic Beauty of Plants" (figure 1.5 p.7)
    and a modified form,
    and save the picture in JPG and PNG format.
    """
    try:
        import numpy
        import PIL.Image
    except ImportError:
        print('Bitmap image is not available!', end=' ', file=sys.stderr)

        return

    turt = opencl_turtle.OpenCLTurtleBitmap(test__turtle.ABOP, 10.0,
                                            90.0, 90.0, dict(),
                                            10.0,
                                            debug=OPENCL_DEBUG)
    turt.save_image('img/abop_figure_1_5_opencl.jpg')
    turt.save_image('img/abop_figure_1_5_opencl.png')
    turt.process()
    turt.save_image('img/abop_figure_1_5_opencl.jpg')
    turt.save_image('img/abop_figure_1_5_opencl.png')


def test__process__canvas():
    """
    Compare the resulting canvas of OpenCLTurtleBitmap.process()
    with the results of TurtleBitmap.process().
    """
    devices = [opencl_l_system.get_device(i, 0) for i in range(2)]

    for filename in sorted(glob.glob(test.EXAMPLES_DIR + '*.txt')):
        name, alphabet, axiom, production = simple_l_system.load_l_system(filename)
        mapping_filename = '{}/turtle/turtle_{}'.format(os.path.dirname(filename),
                                                        os.path.basename(filename))
        try:
            angle_increment, mapping = turtle.load_turtle_mapping(mapping_filename)
        except IOError:
            angle_increment, mapping = 0.0, dict()

        assert 0 <= angle_increment < 360

        if (int(round(angle_increment)) % 90 != 0) or ('[' in alphabet):
            continue

        print('"{}"'.format(name), end=' ', file=sys.stderr)
        sys.stderr.flush()

        for nb_iteration in (0, 1, 2, 5, 6):
            lsystem = fast_l_system.FastLSystem(alphabet, axiom, production)
            lsystem.expand(nb_iteration)
            result = str(lsystem)

            angle_initial = 0.0
            for device in devices:
                for nb_work_group in (1, 123):
                    for nb_work_item in (1, 31, 256):
                        if not opencl_l_system.device_is_ok(device, nb_work_group, nb_work_item):
                            break

                        turt2 = turtle.TurtleBitmap(result, 7.0,
                                                    angle_increment, angle_initial,
                                                    mapping,
                                                    10.0)
                        turt2.process()

                        turt = opencl_turtle.OpenCLTurtleBitmap(result, 7.0,
                                                                angle_increment, angle_initial,
                                                                mapping,
                                                                10.0,
                                                                device=device,
                                                                nb_work_group=nb_work_group,
                                                                nb_work_item=nb_work_item,
                                                                debug=OPENCL_DEBUG)
                        turt.process()

                        assert turt.canvas().shape[0] == turt2.canvas().shape[0]
                        assert turt.canvas().shape[1] == turt2.canvas().shape[1]
                        assert np.array_equal(turt.canvas(), turt2.canvas())

            angle_initial = (angle_initial + 90) % 360


########
# Main #
########
if __name__ == '__main__':
    # Run all test__...() functions (useful if pytest is missing)
    for function in sorted(dir()):
        if function.startswith('test__') and (function != 'test__turtle'):
            print(function, '...', end='', file=sys.stderr)
            sys.stderr.flush()
            locals()[function]()
            print(file=sys.stderr)
    print('===== done =====')
