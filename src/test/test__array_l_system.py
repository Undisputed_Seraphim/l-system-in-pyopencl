#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Tests functions from l_system.array_l_system.py.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 18, 2018
"""

from __future__ import print_function

import sys

import numpy as np

try:
    import pytest
except ImportError:
    print('! pytest module not installed', file=sys.stderr)

import l_system.simple_l_system as simple_l_system

import l_system.array_l_system as array_l_system


def test__alphabet_mapping():
    """
    Compare results of alphabet_mapping() to few values.
    """
    assert array_l_system.alphabet_mapping('') == dict()
    assert array_l_system.alphabet_mapping('X') == {'X': 0}
    assert array_l_system.alphabet_mapping('YX') == {'Y': 0, 'X': 1}
    assert array_l_system.alphabet_mapping('YXA') == {'Y': 0, 'X': 1, 'A': 2}


def test__alphabet_mapping_reverse():
    """
    Compare results of alphabet_mapping_reverse() to few values.
    """
    array = np.array((0, 255, 1, 255))

    assert array_l_system.alphabet_mapping_reverse('YXA', array) == 'YX'
    assert array_l_system.alphabet_mapping_reverse('YXA', array, '_') == 'Y_X_'
    assert array_l_system.alphabet_mapping_reverse('YXA', array, '123') == 'Y123X123'

    assert array_l_system.alphabet_mapping_reverse('YXA', np.array([])) == ''
    assert array_l_system.alphabet_mapping_reverse('YXA', np.array([]), '_') == ''
    assert array_l_system.alphabet_mapping_reverse('YXA', np.array((255, 255))) == ''
    assert array_l_system.alphabet_mapping_reverse('YXA', np.array((255, 255)), '_') == '__'


def test__copy_production_to_production_array():
    """
    Compare results of copy_production_to_production_array() to few values.
    """
    alphabet = 'ABX'
    production = {'A': 'AB',
                  'B': 'A'}
    variables, constants = simple_l_system.split_alphabet(alphabet, production)
    a_mapping = array_l_system.alphabet_mapping(variables + constants)
    r_max_size = simple_l_system.rule_max_size(production)

    assert variables == 'AB'
    assert constants == 'X'
    assert a_mapping == {'A': 0, 'B': 1, 'X': 2}
    assert r_max_size == 2

    production_array = array_l_system.empty_production_array(len(production) * r_max_size)
    production_array.fill(42)
    array_l_system.copy_production_to_production_array(production, a_mapping, r_max_size,
                                                       production_array)

    assert np.array_equal(production_array, np.array([0, 1,
                                                      0, 255]))

    #
    alphabet = 'ABX'
    production = {'A': 'AB',
                  'B': 'AXAX'}
    variables, constants = simple_l_system.split_alphabet(alphabet, production)
    a_mapping = array_l_system.alphabet_mapping(variables + constants)
    r_max_size = simple_l_system.rule_max_size(production)

    assert variables == 'AB'
    assert constants == 'X'
    assert a_mapping == {'A': 0, 'B': 1, 'X': 2}
    assert r_max_size == 4

    production_array = array_l_system.empty_production_array(len(production) * r_max_size)
    production_array.fill(42)
    array_l_system.copy_production_to_production_array(production, a_mapping, r_max_size,
                                                       production_array)

    assert np.array_equal(production_array, np.array([0, 1, 255, 255,
                                                      0, 2, 0, 2]))

    #
    alphabet = 'ABXF'
    production = {'A': 'AB',
                  'F': 'FFFFF',
                  'B': 'AXAX'}
    variables, constants = simple_l_system.split_alphabet(alphabet, production)
    a_mapping = array_l_system.alphabet_mapping(variables + constants)
    r_max_size = simple_l_system.rule_max_size(production)

    assert variables == 'ABF'
    assert constants == 'X'
    assert a_mapping == {'A': 0, 'B': 1, 'F': 2, 'X': 3}
    assert r_max_size == 5

    production_array = array_l_system.empty_production_array(len(production) * r_max_size)
    production_array.fill(42)
    array_l_system.copy_production_to_production_array(production, a_mapping, r_max_size,
                                                       production_array)

    assert np.array_equal(production_array, np.array([0, 1, 255, 255, 255,
                                                      0, 3, 0, 3, 255,
                                                      2, 2, 2, 2, 2]))


def test__default_production_array():
    """
    Init production array of several sizes and check it.
    """
    for size in range(100):
        production_array = array_l_system.default_production_array(size)

        assert isinstance(production_array, np.ndarray)
        assert production_array.shape == (size, )

        for i in range(size):
            assert production_array[i] == array_l_system.DEFAULT


def test__empty_production_array():
    """
    Init production array of several sizes and check it.
    """
    for size in range(100):
        production_array = array_l_system.empty_production_array(size)

        assert isinstance(production_array, np.ndarray)
        assert production_array.shape == (size, )


def test__production_array_expand():
    """
    Compare results of production_array_expand() to few values.
    """
    production_array = np.array([0, 1,
                                 0, 255])
    r_max_size = 2
    nb_rule = 2
    production_expanded_array = array_l_system.empty_production_array(nb_rule * r_max_size**2)
    new_r_expanded_max_size \
        = array_l_system.production_array_expand(nb_rule, production_array, r_max_size,
                                                 production_array, r_max_size,
                                                 production_expanded_array, r_max_size * r_max_size)

    assert new_r_expanded_max_size == 3
    assert np.array_equal(production_expanded_array[:6], np.array([0, 1, 0,
                                                                   0, 1, 255]))

    #
    production_array_to_expand = np.array([0, 1, 0, 255,
                                           0, 1, 255, 255])
    production_array = np.array([0, 1,
                                 0, 255])
    r_max_size_to_expand = 4
    nb_rule = 2
    production_expanded_array = array_l_system.empty_production_array(nb_rule * r_max_size_to_expand * r_max_size)
    new_r_expanded_max_size \
        = array_l_system.production_array_expand(nb_rule, production_array, r_max_size,
                                                 production_array_to_expand, r_max_size_to_expand,
                                                 production_expanded_array, r_max_size_to_expand * r_max_size)

    assert new_r_expanded_max_size == 5
    assert np.array_equal(production_expanded_array[:10], np.array([0, 1, 0, 0, 1,
                                                                    0, 1, 0, 255, 255]))

    #
    production_array_to_expand = np.array([1, 0, 255, 255,
                                           1, 0, 1, 1])
    production_array = np.array([0, 255,
                                 0, 1])
    r_max_size_to_expand = 4
    nb_rule = 2
    production_expanded_array = array_l_system.empty_production_array(nb_rule * r_max_size_to_expand * r_max_size)
    new_r_expanded_max_size \
        = array_l_system.production_array_expand(nb_rule, production_array, r_max_size,
                                                 production_array_to_expand, r_max_size_to_expand,
                                                 production_expanded_array, r_max_size_to_expand * r_max_size)

    assert new_r_expanded_max_size == 7
    assert np.array_equal(production_expanded_array[:14], np.array([0, 1, 0, 255, 255, 255, 255,
                                                                    0, 1, 0, 0, 1, 0, 1]))

    #
    production_array = np.array([1, 255,
                                 2, 255,
                                 0, 1])
    r_max_size = 2
    nb_rule = 3
    production_expanded_array = array_l_system.empty_production_array(nb_rule * r_max_size**2)
    new_r_expanded_max_size \
        = array_l_system.production_array_expand(nb_rule, production_array, r_max_size,
                                                 production_array, r_max_size,
                                                 production_expanded_array, r_max_size * r_max_size)

    assert new_r_expanded_max_size == 2
    assert np.array_equal(production_expanded_array[:6], np.array([2, 255,
                                                                   0, 1,
                                                                   1, 2]))

    #
    production_array = np.array([1, 255,
                                 2, 3,
                                 0, 4])
    r_max_size = 2
    nb_rule = 3
    production_expanded_array = array_l_system.empty_production_array(nb_rule * r_max_size**2)
    new_r_expanded_max_size \
        = array_l_system.production_array_expand(nb_rule, production_array, r_max_size,
                                                 production_array, r_max_size,
                                                 production_expanded_array, r_max_size * r_max_size)

    assert new_r_expanded_max_size == 3
    assert np.array_equal(production_expanded_array[:9], np.array([2, 3, 255,
                                                                   0, 4, 3,
                                                                   1, 4, 255]))


def test__production_array_to_production():
    """
    Compare results of production_array_to_production() to few values.
    """
    alphabet = 'ABX'
    production_array = np.array([0, 1,
                                 0, 255])

    assert array_l_system.production_array_to_production(production_array, alphabet, 2, 2, 2) \
        == {'A': 'AB',
            'B': 'A'}

    #
    alphabet = 'ABX'
    production_array = np.array([0, 1, 255, 255,
                                 0, 2, 0, 2])

    assert array_l_system.production_array_to_production(production_array, alphabet, 2, 4, 4) \
        == {'A': 'AB',
            'B': 'AXAX'}

    #
    variables, constants = 'ABF', 'X'
    production_array = np.array([0, 1, 255, 255, 255,
                                 0, 3, 0, 3, 255,
                                 2, 2, 2, 2, 2])

    assert array_l_system.production_array_to_production(production_array,
                                                         variables + constants,
                                                         3, 5, 5) \
        == {'A': 'AB',
            'F': 'FFFFF',
            'B': 'AXAX'}

    #
    variables, constants = 'ABF', 'X'
    production_array = np.array([0, 1, 255, 255, 255, 255, 255,
                                 0, 3, 0, 3, 255, 255, 255,
                                 2, 2, 2, 2, 2, 255, 255])

    assert array_l_system.production_array_to_production(production_array,
                                                         variables + constants,
                                                         3, 7, 5) \
        == {'A': 'AB',
            'F': 'FFFFF',
            'B': 'AXAX'}


########
# Main #
########
if __name__ == '__main__':
    # Run all test__...() functions (useful if pytest is missing)
    for function in sorted(dir()):
        if function.startswith('test__'):
            print(function, '...', end='', file=sys.stderr)
            sys.stderr.flush()
            locals()[function]()
            print(file=sys.stderr)
    print('===== done =====')
