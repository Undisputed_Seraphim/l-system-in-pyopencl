#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Tests class and subclasses of Turtle from l_system.turtle.py.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 18, 2018
"""

from __future__ import print_function

import sys

try:
    import pytest
except ImportError:
    print('! pytest module not installed', file=sys.stderr)

import l_system.turtle as turtle

ABOP = 'FFF-FF-F-F+F+FF-F-FFF'
ABOP_MODIFIED = 'FfF-FF-F-F+F+FF-f-FFF'


def test__abop_figure_1_5():
    """
    Process the example from "The Algorithmic Beauty of Plants" (figure 1.5 p.7)
    and a modified form.
    """
    turt = turtle.Turtle(ABOP, 10.0,
                         90.0, 90.0, dict(),
                         10.0)
    turt.process()

    assert turt.intwidth() == 50
    assert turt.intheight() == 50

    #
    turt = turtle.Turtle(ABOP_MODIFIED, 10.0,
                         90.0, 20.0, dict(),
                         10.0)
    turt.process()

    assert turt.intwidth() == 56
    assert turt.intheight() == 56


def test__abop_figure_1_5_bitmap():
    """
    Process the example from "The Algorithmic Beauty of Plants" (figure 1.5 p.7)
    and a modified form,
    and save the picture in JPG and PNG format.
    """
    try:
        import numpy
        import PIL.Image
    except ImportError:
        print('Bitmap image is not available!', end=' ', file=sys.stderr)

        return

    turt = turtle.TurtleBitmap(ABOP, 10.0,
                               90.0, 90.0, dict(),
                               10.0)
    turt.save_image('img/abop_figure_1_5.jpg')
    turt.save_image('img/abop_figure_1_5.png')
    turt.process()
    turt.save_image('img/abop_figure_1_5.jpg')
    turt.save_image('img/abop_figure_1_5.png')

    #
    turt = turtle.TurtleBitmap(ABOP_MODIFIED, 10.0,
                               90.0, 20.0, dict(),
                               10.0)
    turt.save_image('img/abop_figure_1_5_modified.png')
    turt.process()
    turt.save_image('img/abop_figure_1_5_modified.png')


def test__abop_figure_1_5_postscript():
    """
    Process the example from "The Algorithmic Beauty of Plants" (figure 1.5 p.7)
    and a modified form,
    and save the pictures in Encapsulated PostScript format.
    """
    turt = turtle.TurtlePostscript(ABOP, 10.0,
                                   90.0, 90.0, dict(),
                                   10.0,
                                   'abop figure 1.5 p.7')
    turt.save_image('img/abop_figure_1_5.eps')
    turt.process()
    turt.save_image('img/abop_figure_1_5.eps')

    #
    turt = turtle.TurtlePostscript(ABOP_MODIFIED, 10.0,
                                   90.0, 20.0, dict(),
                                   10.0,
                                   'modified abop figure 1.5 p.7')
    turt.save_image('img/abop_figure_1_5_modified.eps')
    turt.process()
    turt.save_image('img/abop_figure_1_5_modified.eps')


########
# Main #
########
if __name__ == '__main__':
    # Run all test__...() functions (useful if pytest is missing)
    for function in sorted(dir()):
        if function.startswith('test__'):
            print(function, '...', end='', file=sys.stderr)
            sys.stderr.flush()
            locals()[function]()
            print(file=sys.stderr)
    print('===== done =====')
