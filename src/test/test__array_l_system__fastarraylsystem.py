#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Tests of class FastArrayLSystem from l_system.array_l_system.py.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 18, 2018
"""

from __future__ import print_function

import glob
import sys

try:
    import pytest
except ImportError:
    print('! pytest module not installed', file=sys.stderr)

import test

import l_system.simple_l_system as simple_l_system
import l_system.fast_l_system as fast_l_system

import l_system.array_l_system as array_l_system


def test__expand_algae():
    """
    Compare the expansion of the algae L-system
    to the result from the Wikipedia page.
    """
    production = {'A': 'AB',
                  'B': 'A'}
    algae = array_l_system.FastArrayLSystem('AB', 'A', production)

    assert str(algae) == 'A'

    for i, result in enumerate(test.ALGAE):
        assert algae.iteration() == i
        assert str(algae) == result

        if i < len(test.ALGAE) - 1:
            assert algae.expand_string(str(algae), production) == test.ALGAE[i + 1]
            assert algae.iteration() == i
            assert str(algae) == result

        algae.expand()


def test__expand_example_files():
    """
    Compare the expansion of the test.EXAMPLES L-systems
    to the results from the Wikipedia page.
    """
    for key_name, results in sorted(test.EXAMPLES.items()):
        filename = test.EXAMPLES_DIR + key_name.replace(' ', '_') + '.txt'
        name, alphabet, axiom, production = simple_l_system.load_l_system(filename)

        assert name == key_name

        print('"{}"'.format(name), end=' ', file=sys.stderr)
        sys.stderr.flush()
        lsystem = array_l_system.FastArrayLSystem(alphabet, axiom, production)

        assert str(lsystem) == axiom
        assert str(lsystem) == results[0]

        for i, result in enumerate(results):
            assert lsystem.iteration() == i
            assert str(lsystem) == result

            if i < len(results) - 1:
                assert lsystem.expand_string(str(lsystem),
                                             production) == results[i + 1]
                assert lsystem.iteration() == i
                assert str(lsystem) == result

            lsystem.expand()


def test__expand():
    """
    Compare the expansion of several iterations in one step
    to the results of FastLSystem.expand().
    """
    for filename in sorted(glob.glob(test.EXAMPLES_DIR + '*.txt')):
        name, alphabet, axiom, production = simple_l_system.load_l_system(filename)
        print('"{}"'.format(name), end=' ', file=sys.stderr)
        sys.stderr.flush()
        for size_step in range(7):
            lsystem = array_l_system.FastArrayLSystem(alphabet, axiom, production)
            lsystem.expand(size_step)

            lsystem2 = fast_l_system.FastLSystem(alphabet, axiom, production)
            lsystem2.expand(size_step)

            assert str(lsystem) == str(lsystem2)
            assert lsystem.iteration() == lsystem2.iteration()


def test__production_iterate():
    """
    Compare the result of FastArrayLSystem.production_iterate(nb_iteration)
    with the results of FastLSystem.production_iterate(nb_iteration).
    """
    for filename in sorted(glob.glob(test.EXAMPLES_DIR + '*.txt')):
        name, alphabet, axiom, production = simple_l_system.load_l_system(filename)
        print('"{}"'.format(name), end=' ', file=sys.stderr)
        sys.stderr.flush()

        assert simple_l_system.is_production(production), production

        for nb_iteration in range(7):
            lsystem = array_l_system.FastArrayLSystem(alphabet, axiom, production)
            production_iter = lsystem.production_iterate(nb_iteration)
            result = lsystem.expand_string(lsystem._axiom, production_iter)

            lsystem2 = fast_l_system.FastLSystem(alphabet, axiom, production)
            production_iter2 = lsystem2.production_iterate(nb_iteration)
            result2 = lsystem2.expand_string(lsystem2._axiom, production_iter2)

            assert result == result2

            assert production_iter == production_iter2


########
# Main #
########
if __name__ == '__main__':
    # Run all test__...() functions (useful if pytest is missing)
    for function in sorted(dir()):
        if function.startswith('test__'):
            print(function, '...', end='', file=sys.stderr)
            sys.stderr.flush()
            locals()[function]()
            print(file=sys.stderr)
    print('===== done =====')
