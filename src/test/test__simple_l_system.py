#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Tests functions from l_system.simple_l_system.py.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 18, 2018
"""

from __future__ import print_function

import sys

try:
    import pytest
except ImportError:
    print('! pytest module not installed', file=sys.stderr)

import l_system.simple_l_system as simple_l_system


def test__expanded_size():
    """
    Compare results of expanded_size() to few values.
    """
    assert simple_l_system.expanded_size(1, 10,
                                         1) == 1 * 1 + 9
    assert simple_l_system.expanded_size(3, 10,
                                         5) == 3 * 5 + 7
    assert simple_l_system.expanded_size(10, 10,
                                         7) == 10 * 7


def test__is_production():
    """
    Compare results of is_production() to few values.
    """
    assert simple_l_system.is_production({'A': 'BY',
                                          'B': 'X'})
    assert simple_l_system.is_production({'A': 'BY',
                                          'B': 'AXA'})
    assert simple_l_system.is_production({'A': 'FF',
                                          'B': 'X'})

    assert not simple_l_system.is_production(dict())
    assert not simple_l_system.is_production({'A': ''})
    assert not simple_l_system.is_production({'A': ''})
    assert not simple_l_system.is_production({'A': '',
                                              'B': 'X'})
    assert not simple_l_system.is_production({'A': '',
                                              'B': 'X'})
    assert not simple_l_system.is_production({'A': 'BY',
                                              'B': 'B'})
    assert not simple_l_system.is_production({'': 'FF',
                                              'B': 'X'})
    assert not simple_l_system.is_production({'A': 'FF',
                                              42: 'X'})
    assert not simple_l_system.is_production({'A': 'FF',
                                              'B': 42})


def test__rule_iter_upper_bounds():
    """
    Compare results of rule_iter_upper_bounds() to few values.
    """
    assert simple_l_system.rule_iter_upper_bounds(0, 42, 36, 666) == (1, 0, 1)
    assert simple_l_system.rule_iter_upper_bounds(1, 42, 36, 666) == (42, 36, 666)
    assert simple_l_system.rule_iter_upper_bounds(2, 3, 36, 10) == (3 * 3,
                                                                    36 * (1 + 3),
                                                                    1 + (10 - 1) * (1 + 3))
    assert simple_l_system.rule_iter_upper_bounds(3, 5, 36, 10) == (5 * 5 * 5,
                                                                    36 * (1 + 5 + 25),
                                                                    1 + (10 - 1) * (1 + 5 + 25))
    assert simple_l_system.rule_iter_upper_bounds(42, 1, 36, 666) == (1,
                                                                      36 * 42,
                                                                      1 + (666 - 1) * 42)


def test__rule_iter_upper_nb_constant():
    """
    Compare results of rule_iter_upper_nb_constant() to few values.
    """
    assert simple_l_system.rule_iter_upper_nb_constant(0, 42, 36) == 0
    assert simple_l_system.rule_iter_upper_nb_constant(1, 42, 36) == 36
    assert simple_l_system.rule_iter_upper_nb_constant(2, 3, 36) == 36 * (1 + 3)
    assert simple_l_system.rule_iter_upper_nb_constant(3, 5, 36) == 36 * (1 + 5 + 25)
    assert simple_l_system.rule_iter_upper_nb_constant(42, 1, 36) == 36 * 42


def test__rule_iter_upper_nb_variable():
    """
    Compare results of rule_iter_upper_nb_variable() to few values.
    """
    assert simple_l_system.rule_iter_upper_nb_variable(0, 42) == 1
    assert simple_l_system.rule_iter_upper_nb_variable(1, 42) == 42
    assert simple_l_system.rule_iter_upper_nb_variable(2, 3) == 3 * 3
    assert simple_l_system.rule_iter_upper_nb_variable(3, 5) == 5 * 5 * 5
    assert simple_l_system.rule_iter_upper_nb_variable(42, 1) == 1


def test__rule_iter_upper_size():
    """
    Compare results of rule_iter_upper_size() to few values.
    """
    assert simple_l_system.rule_iter_upper_size(0, 42, 666) == 1
    assert simple_l_system.rule_iter_upper_size(1, 42, 666) == 666
    assert simple_l_system.rule_iter_upper_size(2, 3, 10) == 3 * 3 + (1 + 3) * (10 - 3)
    assert simple_l_system.rule_iter_upper_size(3, 5, 10) == 5 * 5 * 5 + (1 + 5 + 25) * (10 - 5)
    assert simple_l_system.rule_iter_upper_size(42, 1, 666) == 1 + (666 - 1) * 42


def test__rule_max_nb_constant():
    """
    Compare results of rule_max_nb_constant() to few values.
    """
    assert simple_l_system.rule_max_nb_constant('A', dict()) == 1
    assert simple_l_system.rule_max_nb_constant('AX', {'A': 'X'}) == 1
    assert simple_l_system.rule_max_nb_constant('ABX', {'A': 'BBA',
                                                        'B': 'XX'}) == 2
    assert simple_l_system.rule_max_nb_constant('ABXY', {'A': 'BY',
                                                         'B': 'X'}) == 1
    assert simple_l_system.rule_max_nb_constant('ABXY', {'A': 'BY',
                                                         'B': 'AXA'}) == 1
    assert simple_l_system.rule_max_nb_constant('FABX', {'A': 'FF',
                                                         'B': 'XABBA'}) == 2


def test__rule_max_nb_variable():
    """
    Compare results of rule_max_nb_variable() to few values.
    """
    assert simple_l_system.rule_max_nb_variable(dict()) == 0
    assert simple_l_system.rule_max_nb_variable({'A': ''}) == 0
    assert simple_l_system.rule_max_nb_variable({'A': '',
                                                 'B': 'X'}) == 0
    assert simple_l_system.rule_max_nb_variable({'A': 'BY',
                                                 'B': 'X'}) == 1
    assert simple_l_system.rule_max_nb_variable({'A': 'BY',
                                                 'B': 'AXA'}) == 2
    assert simple_l_system.rule_max_nb_variable({'A': 'FF',
                                                 'B': 'X'}) == 0


def test__rule_max_size():
    """
    Compare results of rule_max_size() to few values.
    """
    assert simple_l_system.rule_max_size(dict()) == 0
    assert simple_l_system.rule_max_size({'A': ''}) == 0
    assert simple_l_system.rule_max_size({'A': '',
                                          'B': 'X'}) == 1
    assert simple_l_system.rule_max_size({'A': 'FF',
                                          'B': 'X'}) == 2


def test__string_iter_upper_bounds():
    """
    Check results of string_iter_upper_bounds() on few examples.
    """
    assert simple_l_system.string_iter_upper_bounds(0,
                                                    42, 36, 666,
                                                    2, 3, 4) == (2,
                                                                 3,
                                                                 4)
    assert simple_l_system.string_iter_upper_bounds(1,
                                                    42, 36, 666,
                                                    2, 3, 4) == (2 * 42,
                                                                 3 + 2 * 36,
                                                                 4 + 2 * (666 - 1))
    assert simple_l_system.string_iter_upper_bounds(2,
                                                    42, 36, 666,
                                                    2, 3, 4) == (2 * 42**2,
                                                                 3 + 2 * 36 * (1 + 42),
                                                                 4 + 2 * (666 - 1) * (1 + 42))
    assert simple_l_system.string_iter_upper_bounds(3,
                                                    42, 36, 666,
                                                    2, 3, 4) == (2 * 42**3,
                                                                 3 + 2 * 36 * (1 + 42 + 42**2),
                                                                 4 + 2 * (666 - 1) * (1 + 42 + 42**2))
    assert simple_l_system.string_iter_upper_bounds(42,
                                                    1, 36, 666,
                                                    2, 3, 4) == (2,
                                                                 3 + 2 * 36 * 42,
                                                                 4 + 2 * (666 - 1) * 42)


def test__split_alphabet():
    """
    Check results of split_alphabet() on few examples.
    """
    production = {'A': 'AB',
                  'B': 'A'}

    assert simple_l_system.split_alphabet('', production) == ('', '')
    assert simple_l_system.split_alphabet('A', production) == ('A', '')
    assert simple_l_system.split_alphabet('B', production) == ('B', '')
    assert simple_l_system.split_alphabet('AB', production) == ('AB', '')
    assert simple_l_system.split_alphabet('BA', production) == ('BA', '')
    assert simple_l_system.split_alphabet('ZABC', production) == ('AB', 'ZC')

    #
    production = {'A': 'AB',
                  'X': 'B'}

    assert simple_l_system.split_alphabet('', production) == ('', '')
    assert simple_l_system.split_alphabet('A', production) == ('A', '')
    assert simple_l_system.split_alphabet('B', production) == ('', 'B')
    assert simple_l_system.split_alphabet('AB', production) == ('A', 'B')
    assert simple_l_system.split_alphabet('BA', production) == ('A', 'B')
    assert simple_l_system.split_alphabet('ZABC', production) == ('A', 'ZBC')
    assert simple_l_system.split_alphabet('XABC', production) == ('XA', 'BC')


def test__split_string():
    """
    Check results of split_string() on few examples.
    """
    production = {'A': 'AB',
                  'B': 'A'}

    assert simple_l_system.split_string('', 'AB', production) == ('', '')
    assert simple_l_system.split_string('A', 'AB', production) == ('A', '')
    assert simple_l_system.split_string('B', 'AB', production) == ('B', '')
    assert simple_l_system.split_string('AB', 'AB', production) == ('AB', '')
    assert simple_l_system.split_string('BA', 'AB', production) == ('BA', '')
    assert simple_l_system.split_string('ZABC', 'ABCZ', production) == ('AB', 'ZC')

    #
    production = {'A': 'AB',
                  'X': 'A'}

    assert simple_l_system.split_string('', 'AB', production) == ('', '')
    assert simple_l_system.split_string('A', 'AB', production) == ('A', '')
    assert simple_l_system.split_string('B', 'AB', production) == ('', 'B')
    assert simple_l_system.split_string('AB', 'AB', production) == ('A', 'B')
    assert simple_l_system.split_string('BA', 'AB', production) == ('A', 'B')
    assert simple_l_system.split_string('ZABC', 'ABCZ', production) == ('A', 'ZBC')
    assert simple_l_system.split_string('ZABXZCAX', 'ABCZ', production) == ('AXAX', 'ZBZC')


########
# Main #
########
if __name__ == '__main__':
    # Run all test__...() functions (useful if pytest is missing)
    for function in sorted(dir()):
        if function.startswith('test__'):
            print(function, '...', end='', file=sys.stderr)
            sys.stderr.flush()
            locals()[function]()
            print(file=sys.stderr)
    print('===== done =====')
