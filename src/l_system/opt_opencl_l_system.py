# -*- coding: latin-1 -*-

"""
Same as FastOpenCLLSystem
but with the optimized kernel from opt_l_system.cl.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 17, 2018
"""

from __future__ import division
from __future__ import print_function

import l_system.opencl_l_system as opencl_l_system


class OptFastOpenCLLSystem(opencl_l_system.FastOpenCLLSystem):
    """
    Same as FastOpenCLLSystem
    but with the optimized kernel from opt_l_system.cl.
    """
    def __init__(self, alphabet, axiom, production, name='',
                 kernel_filename=None,
                 device=None, nb_work_item=4,
                 debug=None):
        """
        Initialize the L-system (alphabet, axiom, production rules).

        Alphabet is maybe reordered to beginning by symbols of production rules.

        Production rule can't be constant.
        And at least one rule must be contains at least two symbols.

        :param alphabet: str of unique symbols
        :param axiom: str of initial symbols
        :param production: dict symbol: (str of symbols)
        :param name: str
        :param kernel_filename: None or str
        :param device: None or OpenCL device
        :param nb_work_item: number of rules <= int <= maximum number of work items supported by device
        :param debug: None or bool
        """
        assert (kernel_filename is None) or isinstance(kernel_filename, str), type(kernel_filename)

        assert isinstance(nb_work_item, int), type(nb_work_item)
        assert nb_work_item >= 1, nb_work_item
        assert nb_work_item >= len(production), (nb_work_item, len(production))

        assert (debug is None) or isinstance(debug, bool), type(debug)

        opencl_l_system.FastOpenCLLSystem.__init__(self, alphabet, axiom, production, name=name,
                                                   kernel_filename=('opt_l_system.cl' if kernel_filename is None
                                                                    else kernel_filename),
                                                   device=device, nb_work_item=nb_work_item,
                                                   debug=debug)
