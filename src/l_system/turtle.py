# -*- coding: latin-1 -*-

"""
Turtle interpretation for L-systems.

See https://en.wikipedia.org/wiki/L-system

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 17, 2018
"""

from __future__ import division
from __future__ import print_function

import math

import l_system
import l_system.simple_l_system as simple_l_system


def load_turtle_mapping(filename):
    """
    Load from a file the description used to draw picture from a L-system,
    and return (initial angle, mapping to convert symbol of the L-system).

    The format of the file must be:

| ``initial angle in degree``
| ``symbol: string of turtle symbol``
| ``symbol: string of turtle symbol``
| ``...``

    Turtle symbols are explained in the Turtle class documentation.

    :param filename: str, filename to a readable valid file

    :return: (float, dict symbol: (str of symbols))
    """
    assert isinstance(filename, str), type(filename)
    assert filename != ''

    with open(filename) as filein:
        angle_increment = float(filein.readline().strip())

        mapping = dict()
        for line in filein:
            pieces = line.strip().split(':')

            assert len(pieces) == 2, pieces

            symbol = pieces[0].strip()
            mapped = pieces[1].strip()

            assert simple_l_system.is_symbol(symbol) and (mapped != '')
            assert symbol not in mapping

            mapping[symbol] = mapped

    assert 0 <= angle_increment < 360

    return (angle_increment, mapping)


def save_bitmap(canvas, filename):
    """
    Save the image represented by a NumPy boolean array to a file,
    in a PNG or JPEG format
    specified by the extension in filename (.png or .jpg).

    :param canvas: NumPy 2D array of booleans
    :param filename: str, filename to a writable file
    """
    import numpy as np
    import PIL.Image

    assert isinstance(canvas, np.ndarray), type(canvas)

    assert isinstance(filename, str), type(filename)
    assert filename != ''

    image = PIL.Image.fromarray(np.uint8(canvas) * 255).convert('1')
    image.save(filename)


class Turtle:
    """
    Mutable implementation of the turtle interpretation
    from a string of symbols.

    Base class for TurtleBitmap and TurtlePostscript.

    The string of symbols is first mapped with the mapping dictionary.

    Orientation is the standard counter-clockwise, in degree:

    |      90
    |       |
    | 180 --+-- 0
    |       |
    |      270

    Symbol recognized:
    'F': draw a line of length distance_increment
    'f': move of length distance_increment
    '+': turn left of angle_increment degree
    '-': turn right of angle_increment degree
    '[': push the current position and the current angle on the stack
    ']': pop the stack and restore the current position and the current orientation

    See "The Algorithmic Beauty of Plants" p.6 and p.209
    http://algorithmicbotany.org/papers/abop/abop.pdf

    :example:
      Turtle('1111[11[1[0]0]1[0]0]11[1[0]0]1[0]0', 10.0, 45.0, 90.0, {'0': 'F', '1': 'F', '[': '[-', ']': ']+'}, 10.0)
      for the representation of the 3rd step of the fractal binary tree
      https://en.wikipedia.org/wiki/L-system#Example_2:_Fractal_(binary)_tree
    """

    def __init__(self, string, distance_increment,
                 angle_increment, angle_initial, mapping, margin):
        """
        Initialize the state to initial data.

        :param string: str of symbols
        :param distance_increment: float > 0
        :param angle_increment: 0 <= float < 360
        :param angle_initial: 0 <= float < 360
        :param mapping: dict symbol: (str of symbols)
        :param margin: float >= 1
        """
        assert isinstance(string, str), type(string)

        assert isinstance(distance_increment, float), type(distance_increment)
        assert distance_increment > 0, distance_increment

        assert isinstance(angle_increment, float), type(angle_increment)
        assert 0 <= angle_increment < 360, angle_increment

        assert isinstance(angle_initial, float), type(angle_initial)
        assert 0 <= angle_initial < 360, angle_initial

        assert isinstance(mapping, dict), type(mapping)

        assert isinstance(margin, float), type(margin)
        assert margin >= 1, margin

        self._string = string
        self._distance_increment = distance_increment
        self._angle_initial = angle_initial
        self._angle_increment = angle_increment
        self._mapping = dict(mapping)
        self._margin = margin

        self._mapped = ''.join(mapping.get(symbol, symbol)
                               for symbol in string)

        self._x = 0.0
        self._y = 0.0

        self._x_min = self._x
        self._y_min = self._y
        self._x_max = self._x
        self._y_max = self._y

        self._angle = angle_initial

        self._stack = []

    def __str__(self):
        """
        Return a string representation of information
        about size and current state.

        :return: str
        """
        return ('Min ({:.3f}, {:.3f}) to max ({:.3f}, {:.3f}) Size ({:.3f}, {:.3f}) ({}, {}) Current ({:.3f}, {:.3f}) {:.3f}'
                .format(self._x_min, self._y_min, self._x_max, self._y_max,
                        self.width(), self.height(),
                        self.intwidth(), self.intheight(),
                        self._x, self._y, self._angle))

    def forward_only(self):
        """
        Change the current position by a move of _distance_increment.
        """
        radian = math.radians(self._angle)
        self._x += self._distance_increment * math.cos(radian)
        self._y += self._distance_increment * math.sin(radian)

    def forward(self):
        """
        Change the current position by a move of _distance_increment
        and update the minimum and maximum x and y values.
        """
        self.forward_only()

        self._x_min = min(self._x_min, self._x)
        self._y_min = min(self._y_min, self._y)
        self._x_max = max(self._x_max, self._x)
        self._y_max = max(self._y_max, self._y)

    def height(self):
        """
        Return the height of the picture.

        :return: float
        """
        return self._y_max - self._y_min + self._margin * 2

    def intheight(self):
        """
        Return the integer height of the picture.

        :return: int
        """
        return simple_l_system.intceil(self.height())

    def intwidth(self):
        """
        Return the integer width of the picture.

        :return: int
        """
        return simple_l_system.intceil(self.width())

    def left(self):
        """
        Change the current direction
        by a rotation of _angle_increment to the left.
        """
        self._angle = (self._angle + self._angle_increment) % 360

    def mapped(self):
        """
        :return: the string converted to the turtle interpretation
        """
        return self._mapped

    def print(self):
        """
        Print the canvas in text mode
        (a ' ' for blank and a 'x' for a point).
        """
        for y in range(self._canvas.shape[0]):
            for x in range(self._canvas.shape[1]):
                print(' ' if self._canvas[y, x] else 'x', end='')
            print()

    def pop_current(self):
        """
        Pop from the not empty stack
        the last position and the last orientation saved
        and restore the current position and the current orientation.
        """
        assert self._stack  # not empty

        self._x, self._y, self._angle = self._stack.pop()

    def process(self):
        """
        Process the sequence of symbols given when initializing the object.
        """
        self.process_string(self._mapped)

    def process_string(self, string):
        """
        Process a sequence of symbols.

        :param string: str of symbols
        """
        assert isinstance(string, str), type(string)

        for symbol in string:
            self.process_symbol(symbol)

    def process_symbol(self, symbol):
        """
        Process a symbol like explained in the Turtle class documentation.

        :param symbol: symbol
        """
        assert simple_l_system.is_symbol(symbol), symbol

        if symbol == 'F':
            self.forward()
        elif symbol == 'f':
            self.forward_only()
        elif symbol == '+':
            self.left()
        elif symbol == '-':
            self.right()
        elif symbol == '[':
            self.push_current()
        elif symbol == ']':
            self.pop_current()

    def push_current(self):
        """
        Push on the stack the current position and the current orientation.
        """
        self._stack.append((self._x, self._y, self._angle))

    def right(self):
        """
        Change the current direction
        by a rotation of _angle_increment to the right.
        """
        self._angle = (self._angle - self._angle_increment) % 360

    def translate_to_positive(self):
        """
        Translate the current position
        and the minimum and maximum x and y values
        to have only positive positions.
        """
        offset_x = self._margin - self._x_min
        offset_y = self._margin - self._y_min

        self._x += offset_x
        self._y += offset_y

        self._x_max += offset_x
        self._y_max += offset_y

        self._x_min = self._margin
        self._y_min = self._margin

        return (offset_x, offset_y)

    def width(self):
        """
        Return the width of the picture.

        :return: float
        """
        return self._x_max - self._x_min + self._margin * 2


class TurtleBitmap(Turtle):
    """
    Turtle interpretation from a string of symbols
    to draw a bitmap image (PNG or JPEG).

    The empty is filled with True values
    and drawing use False values,
    to build a picture in black on white.
    """

    def __init__(self, string, distance_increment,
                 angle_increment, angle_initial, mapping, margin):
        """
        Initialize the state to initial data.

        distance_increment will be rounded (ceil) to a int.

        :param string: str of symbols
        :param distance_increment: float > 0
        :param angle_increment: 0 <= float < 360
        :param angle_initial: 0 <= float < 360
        :param mapping: dict symbol: (str of symbols)
        :param margin: float >= 0
        """
        assert isinstance(string, str), type(string)

        assert isinstance(distance_increment, float), type(distance_increment)
        assert distance_increment > 0, distance_increment

        assert isinstance(angle_increment, float), type(angle_increment)
        assert 0 <= angle_increment < 360, angle_increment

        assert isinstance(angle_initial, float), type(angle_initial)
        assert 0 <= angle_initial < 360, angle_initial

        assert isinstance(mapping, dict), type(mapping)

        assert isinstance(margin, float), type(margin)
        assert margin >= 0, margin

        Turtle.__init__(self, string, float(simple_l_system.intceil(distance_increment)),
                        angle_increment, angle_initial, mapping, margin)

        # Process without draw to compute size of picture
        Turtle.process(self)
        offset_x, offset_y = Turtle.translate_to_positive(self)

        # Reset
        self._x = offset_x
        self._y = offset_y
        self._angle = angle_initial
        self._stack = []

        # Create empty canvas (for image width x height)
        import numpy as np

        self._canvas = np.ones((self.intheight(), self.intwidth()),
                               dtype=bool)

    def canvas(self):
        """
        Return the current canvas.

        :return: NumPy 2D array of booleans
        """
        return self._canvas

    def draw_line(self, x0, y0, x1, y1):
        """
        Draw a line from (x0, y0) to (x1, y1) in the canvas.

        :param x0: 0 <= float < width()
        :param y0: 0 <= float < height()
        :param x1: 0 <= float < width()
        :param y1: 0 <= float < height()
        """
        assert isinstance(x0, float), type(x0)
        assert 0 <= x0 < self.width(), (x0, self.width())

        assert isinstance(y0, float), type(y0)
        assert 0 <= y0 < self.height(), (y0, self.height())

        assert isinstance(x1, float), type(x1)
        assert 0 <= x1 < self.width(), (x1, self.width())

        assert isinstance(y1, float), type(y1)
        assert 0 <= y1 < self.height(), (y1, self.height())

        # Bresenham's line algorithm from
        # http://rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm#Python
        dx = abs(x1 - x0)
        dy = abs(y1 - y0)

        x, y = x0, y0
        sx = (-1 if x0 > x1 else 1)
        sy = (-1 if y0 > y1 else 1)
        if dx > dy:
            err = dx / 2
            while simple_l_system.intround(x) != simple_l_system.intround(x1):
                self.draw_point(x, y)
                err -= dy
                if err < 0:
                    y += sy
                    err += dx
                x += sx
        else:
            err = dy / 2
            while simple_l_system.intround(y) != simple_l_system.intround(y1):
                self.draw_point(x, y)
                err -= dx
                if err < 0:
                    x += sx
                    err += dy
                y += sy
        self.draw_point(x, y)

    def draw_point(self, x, y):
        """
        Draw the point (x, y) in the canvas.

        :param x: 0 <= float < width()
        :param y: 0 <= float < height()
        """
        assert isinstance(x, float), type(x)
        assert 0 <= x < self.width(), (x, self.width())

        assert isinstance(y, float), type(y)
        assert 0 <= y < self.height(), (y, self.height())

        int_x = simple_l_system.intround(x)
        int_y = simple_l_system.intround(self.intheight() - 1 - y)

        assert 0 <= int_x < self._canvas.shape[1], (x, int_x,
                                                    self._canvas.shape[1])
        assert 0 <= int_y < self._canvas.shape[0], (y, int_y,
                                                    self._canvas.shape[0])

        self._canvas[int_y, int_x] = False  # point (x, y)

    def forward(self):
        """
        Change the current position by a move of _distance_increment
        and draw the corresponding line to the canvas.
        """
        if hasattr(self, '_canvas'):  # after creation of canvas, draw
            x0 = self._x
            y0 = self._y
            self.forward_only()
            self.draw_line(x0, y0, self._x, self._y)
        else:                         # before creation of canvas, compute size
            Turtle.forward(self)

    def save_image(self, filename):
        """
        Save the canvas to a file,
        in a PNG or JPEG format
        specified by the extension in filename (.png or .jpg).

        :param filename: str, filename to a writable file
        """
        assert isinstance(filename, str), type(filename)
        assert filename != ''

        save_bitmap(self._canvas, filename)


class TurtlePostscript(Turtle):
    """
    Turtle interpretation from a string of symbols
    to draw a vectorial image (Encapsulated PostScript).
    """

    def __init__(self, string, distance_increment,
                 angle_increment, angle_initial, mapping, margin,
                 name):
        """
        Initialize the state to initial data.

        :param string: str of symbols
        :param distance_increment: float > 0
        :param angle_increment: 0 <= float < 360
        :param angle_initial: 0 <= float < 360
        :param mapping: dict symbol: (str of symbols)
        :param margin: float >= 0
        :param name: str
        """
        assert isinstance(string, str), type(string)

        assert isinstance(distance_increment, float), type(distance_increment)
        assert distance_increment > 0, distance_increment

        assert isinstance(angle_increment, float), type(angle_increment)
        assert 0 <= angle_increment < 360, angle_increment

        assert isinstance(angle_initial, float), type(angle_initial)
        assert 0 <= angle_initial < 360, angle_initial

        assert isinstance(mapping, dict), type(mapping)

        assert isinstance(margin, float), type(margin)
        assert margin >= 0, margin

        assert isinstance(name, str), type(name)

        Turtle.__init__(self, string, float(simple_l_system.intceil(distance_increment)),
                        angle_increment, angle_initial, mapping, margin)

        self._name = name

        self._postscript = []

    def forward(self):
        """
        Change the current position by a move of _distance_increment
        and draw the corresponding line.
        """
        x0 = self._x
        y0 = self._y
        Turtle.forward(self)
        self._postscript.append('{:.3f} {:.3f} {:.3f} {:.3f} l'
                                .format(self._x, self._y, x0, y0))

    def save_image(self, filename):
        """
        Save the canvas to a file,
        in the Encapsulated PostScript format.

        :param filename: str, filename to a writable file
        """
        assert isinstance(filename, str), type(filename)
        assert filename != ''

        params = {'version': l_system.VERSION,
                  'name': self._name,
                  'x0': self._x_min, 'y0': self._y_min,
                  'width': self.width(), 'height': self.height(),
                  'distance_increment': self._distance_increment,
                  'angle_increment': self._angle_increment,
                  'angle_initial': self._angle_initial,
                  'margin': self._margin}

        with open(filename, 'w') as fileout:
            print("""%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: 0 0 {width:.3f} {height:.3f}
%%Title: L-system "{name}"
%%Creator: Python package l_system {version} --- Olivier Pirson --- http://www.opimedia.be/
%%EndComments
save

% Function
%%%%%%%%%%
/l {{  % x1 y1 x0 y0
    moveto lineto stroke
}} def

% Main
%%%%%%
{x0:.3f} neg {y0:.3f} neg translate
{margin:.3f} {margin:.3f} translate
""".format(**params), file=fileout)

            for line in self._postscript:
                print(line, file=fileout)

            print("""
showpage

restore
%%EOF""", file=fileout)
