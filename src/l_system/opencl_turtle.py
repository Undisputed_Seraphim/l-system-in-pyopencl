# -*- coding: latin-1 -*-

"""
Reimplementation of turtle.Turtle with OpenCL

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 19, 2018
"""

from __future__ import division
from __future__ import print_function

import sys

import numpy as np
import pyopencl as cl

import l_system.simple_l_system as simple_l_system
import l_system.array_l_system as array_l_system
import l_system.turtle as turtle

from l_system.opencl_l_system import KERNEL_PATH


class OpenCLTurtleBitmap(turtle.TurtleBitmap):
    """
    Reimplementation of turtle.Turtle with OpenCL.

    Compared to turtle.Turtle this OpenCL implementation
    has these limitations:

    * distance_increment, angle_increment and angle_initial are rounded to int
    * angle_increment and angle_initial are rounded to multiple of 90�
    * don't recognize ']' and ']' symbols and don't has a stack
    """

    def __init__(self, string, distance_increment,
                 angle_increment, angle_initial, mapping, margin,
                 kernel_filename=None,
                 device=None, nb_work_group=2, nb_work_item=4,
                 debug=None):
        """
        Initialize the state to initial data.

        distance_increment, angle_increment and angle_initial are rounded to int
        and angle_increment and angle_initial are rounded to multiple of 90�.

        :param string: str of symbols
        :param distance_increment: float > 0
        :param angle_increment: 0 <= float < 360
        :param angle_initial: 0 <= float < 360
        :param mapping: dict symbol: (str of symbols)
        :param margin: float >= 0
        :param kernel_filename: None or str
        :param device: None or OpenCL device
        :param nb_work_group: int >= 1
        :param nb_work_item: int >= 1
        :param debug: None or bool
        """
        assert isinstance(string, str), type(string)

        assert isinstance(distance_increment, float), type(distance_increment)
        assert distance_increment > 0, distance_increment

        assert isinstance(angle_increment, float), type(angle_increment)
        assert 0 <= angle_increment < 360, angle_increment

        assert isinstance(angle_initial, float), type(angle_initial)
        assert 0 <= angle_initial < 360, angle_initial

        assert isinstance(mapping, dict), type(mapping)

        assert isinstance(margin, float), type(margin)
        assert margin >= 0, margin

        assert isinstance(nb_work_group, int), type(nb_work_group)
        assert nb_work_group >= 1, nb_work_group

        assert isinstance(nb_work_item, int), type(nb_work_item)
        assert nb_work_item >= 1, nb_work_item

        assert (kernel_filename is None) or isinstance(kernel_filename, str), type(kernel_filename)

        assert (debug is None) or isinstance(debug, bool), type(debug)

        distance_increment = float(simple_l_system.intround(distance_increment))

        angle_increment = simple_l_system.intround(angle_increment / 90) * 90.0
        angle_initial = simple_l_system.intround(angle_initial / 90) * 90.0

        turtle.TurtleBitmap.__init__(self, string, distance_increment,
                                     angle_increment, angle_initial, mapping, margin)

        # OpenCL params and kernel
        self._cl_duration_ns = 0
        self._cl_nb_work_group = nb_work_group
        self._cl_nb_work_item = nb_work_item

        self._cl_context = (cl.create_some_context() if device is None
                            else cl.Context((device, )))

        options = ['-I', KERNEL_PATH,
                   '-D', 'ANGLE_INCREMENT={}'.format(simple_l_system.intround(angle_increment)),
                   '-D', 'DISTANCE_INCREMENT={}'
                         .format(simple_l_system.intround(distance_increment))]

        self._cl_kernel = open(KERNEL_PATH + ('turtle.cl' if kernel_filename is None
                                              else kernel_filename)).read()

        self._cl_ndebug = (not __debug__ if debug is None
                           else not debug)

        if self._cl_ndebug:
            options.extend(('-D', 'NDEBUG'))
        else:
            print('OpenCL in DEBUG mode for turtle!', file=sys.stderr)

        self._cl_program = cl.Program(self._cl_context, self._cl_kernel).build(options=options)

    def cl_duration_ns(self):
        """
        Return the duration used by the OpenCL kernel, in nanoseconds ns = 10^{-9} s.

        :return: int
        """
        return self._cl_duration_ns

    def cl_duration_s(self):
        """
        Return the duration used by the OpenCL kernel, in seconds.

        :return: float
        """
        return float(self._cl_duration_ns) / (10**9)

    def process(self):
        """
        Process the sequence of symbols given when initializing the object.
        """
        int_distance_increment = simple_l_system.intround(self._distance_increment)
        int_angle_increment = simple_l_system.intround(self._angle_increment)

        # ### First phase, to compute size of the image and good starting position ###
        total_nb_work_item = self._cl_nb_work_group * self._cl_nb_work_item

        # Array for the string
        string_array = np.fromiter((ord(symbol)
                                   for symbol in self._mapped), np.uint8)
        size = len(self._mapped)

        # Array for "size" results
        h_result = np.empty(7 * total_nb_work_item).astype(np.int32)
        if __debug__:
            h_result.fill(666)  # fake value to be sure to receive a result from the kernel

        # OpenCL context and queue
        queue = cl.CommandQueue(self._cl_context,
                                properties=cl.command_queue_properties.PROFILING_ENABLE)

        # OpenCL buffers
        mf = cl.mem_flags
        d_string = cl.Buffer(self._cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR,
                             hostbuf=string_array)
        d_result = cl.Buffer(self._cl_context, mf.WRITE_ONLY | mf.COPY_HOST_PTR,
                             hostbuf=h_result)

        # Run OpenCL kernel
        f = self._cl_program.compute_size
        if self._cl_ndebug:  # normal execution
            f.set_scalar_arg_dtypes((None, np.uint32,
                                     np.uint32, np.uint32,
                                     None))
            event = f(queue, (total_nb_work_item, ), (self._cl_nb_work_item, ),
                      d_string, size,
                      int_distance_increment, int_angle_increment,
                      d_result)
        else:                # execution with param for OpenCL assertions result
            h_assert = np.empty(2).astype(np.uint32)
            h_assert.fill(-1)
            d_assert = cl.Buffer(self._cl_context, mf.WRITE_ONLY | mf.COPY_HOST_PTR,
                                 hostbuf=h_assert)

            f.set_scalar_arg_dtypes((None, np.uint32,
                                     np.uint32, np.uint32,
                                     None,
                                     None))
            event = f(queue, (total_nb_work_item, ), (self._cl_nb_work_item, ),
                      d_string, size,
                      int_distance_increment, int_angle_increment,
                      d_result,
                      d_assert)

        queue.finish()

        self._cl_duration_ns += event.profile.end - event.profile.start

        # Get results
        cl.enqueue_copy(queue, h_result, d_result)

        if not self._cl_ndebug:  # some assertion failed in OpenCL
            cl.enqueue_copy(queue, h_assert, d_assert)
            if h_assert[0] != 0:
                print('Assertion failed in the OpenCL 1st phase kernel! Maybe line: {} with value: {}'
                      .format(*h_assert))

                exit(1)
            elif h_assert[1] != 0:
                print('1st phase INFO value: {}'.format(h_assert[1]))

        # ### Collect all partial sizes ###
        int_margin = simple_l_system.intround(self._margin)
        x = 0
        y = 0
        angle = simple_l_system.intround(self._angle_initial)
        x_min = 0
        y_min = 0
        x_max = 0
        y_max = 0
        initials = []

        def rotate(x_min, y_min, x_max, y_max, x, y, angle):
            """
            Rotate coordinates with angle.
            """
            assert isinstance(x_min, int), type(x_min)
            assert isinstance(y_min, int), type(y_min)
            assert isinstance(x_max, int), type(x_max)
            assert isinstance(y_max, int), type(y_max)
            assert isinstance(angle, int), type(angle)

            assert x_min <= x_max
            assert y_min <= y_max

            if angle == 0:
                return (x_min, y_min, x_max, y_max, x, y)
            elif angle == 90:   # x <- -y and y <-  x
                return (-y_max, x_min, -y_min, x_max, -y, x)
            elif angle == 180:  # x <- -x and y <- -y
                return (-x_max, -y_max, -x_min, -y_min, -x, -y)
            else:         # 270%: x <-  y and y <- -x
                assert angle == 270, angle

                return (y_min, -x_max, y_max, -x_min, y, -x)

        for i in range(total_nb_work_item):
            assert 0 <= angle < 360, angle

            initials.append((x, y, angle))
            offset = i * 7
            partial_x_min, partial_y_min, \
                partial_x_max, partial_y_max, \
                partial_x, partial_y, partial_angle = [int(n) for n in h_result[offset:offset + 7]]

            partial_x_min, partial_y_min, \
                partial_x_max, partial_y_max, \
                partial_x, partial_y = rotate(partial_x_min, partial_y_min,
                                              partial_x_max, partial_y_max,
                                              partial_x, partial_y, angle)

            assert partial_x_min <= partial_x_max
            assert partial_y_min <= partial_y_max

            angle = (angle + partial_angle) % 360
            x_min = min(x_min, partial_x_min + x)
            y_min = min(y_min, partial_y_min + y)
            x_max = max(x_max, partial_x_max + x)
            y_max = max(y_max, partial_y_max + y)
            x += partial_x
            y += partial_y

        width = x_max - x_min + int_margin * 2
        height = y_max - y_min + int_margin * 2

        # ### Second phase, build picture in the canvas array ###

        # Array for initials positions and angles
        initials = [(x - x_min + int_margin,
                     y - y_min + int_margin,
                     angle)
                    for (x, y, angle) in initials]
        h_initials = np.fromiter([value for triple in initials for value in triple], np.uint32)

        # Array for canvas
        self._canvas = np.full((height, width), True).astype(np.bool)  # dimensions of NumpPy arrays are inverted

        # OpenCL context and queue
        queue = cl.CommandQueue(self._cl_context,
                                properties=cl.command_queue_properties.PROFILING_ENABLE)

        # OpenCL buffers
        mf = cl.mem_flags
        d_string = cl.Buffer(self._cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR,
                             hostbuf=string_array)
        d_initials = cl.Buffer(self._cl_context, mf.READ_ONLY | mf.COPY_HOST_PTR,
                               hostbuf=h_initials)
        d_canvas = cl.Buffer(self._cl_context, mf.WRITE_ONLY | mf.COPY_HOST_PTR,
                             hostbuf=self._canvas)

        # Run OpenCL kernel
        f = self._cl_program.compute_draw
        if self._cl_ndebug:  # normal execution
            f.set_scalar_arg_dtypes((None, np.uint32,
                                     np.uint32, np.uint32,
                                     np.uint32, np.uint32,
                                     None,
                                     None))
            event = f(queue, (total_nb_work_item, ), (self._cl_nb_work_item, ),
                      d_string, size,
                      int_distance_increment, int_angle_increment,
                      width, height,
                      d_initials,
                      d_canvas)
        else:                # execution with param for OpenCL assertions result
            h_assert.fill(-1)
            d_assert = cl.Buffer(self._cl_context, mf.WRITE_ONLY | mf.COPY_HOST_PTR,
                                 hostbuf=h_assert)

            f.set_scalar_arg_dtypes((None, np.uint32,
                                     np.uint32, np.uint32,
                                     np.uint32, np.uint32,
                                     None,
                                     None,
                                     None))
            event = f(queue, (total_nb_work_item, ), (self._cl_nb_work_item, ),
                      d_string, size,
                      int_distance_increment, int_angle_increment,
                      width, height,
                      d_initials,
                      d_canvas,
                      d_assert)

        queue.finish()

        self._cl_duration_ns += event.profile.end - event.profile.start

        # Get results
        cl.enqueue_copy(queue, self._canvas, d_canvas)

        if not self._cl_ndebug:  # some assertion failed in OpenCL
            cl.enqueue_copy(queue, h_assert, d_assert)
            if h_assert[0] != 0:
                print('Assertion failed in the OpenCL 2nd phase kernel! Maybe line: {} with value: {}'
                      .format(*h_assert))

                exit(1)
            elif h_assert[1] != 0:
                print('2nd phase INFO value: {}'.format(h_assert[1]))
