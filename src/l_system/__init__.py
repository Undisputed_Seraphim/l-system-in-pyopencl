# -*- coding: latin-1 -*-

"""
Package L-system (Lindenmayer system) generator.

See https://en.wikipedia.org/wiki/L-system

Tested with

* Python 2.7.13
* Python 3.5.3
* partially (neither NumPy or OpenCL) PyPy 5.6.0 (Python 2.7.12)

on Debian GNU/Linux 9.5 (stretch).

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 17, 2018

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

VERSION = '01.00.00'
