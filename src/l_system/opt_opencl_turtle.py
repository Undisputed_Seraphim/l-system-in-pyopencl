# -*- coding: latin-1 -*-

"""
Same as OpenCLTurtleBitmap
but with the optimized kernel from opt_turtle.cl.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 19, 2018
"""

from __future__ import division
from __future__ import print_function

import l_system.opencl_turtle as opencl_turtle


class OptOpenCLTurtleBitmap(opencl_turtle.OpenCLTurtleBitmap):
    """
    Same as OpenCLTurtleBitmap (with same limitations)
    but with the optimized kernel from opt_turtle.cl.
    """

    def __init__(self, string, distance_increment,
                 angle_increment, angle_initial, mapping, margin,
                 kernel_filename=None,
                 device=None, nb_work_group=2, nb_work_item=4,
                 debug=None):
        """
        Initialize the state to initial data.

        distance_increment, angle_increment and angle_initial are rounded to int
        and angle_increment and angle_initial are rounded to multiple of 90�.

        For an unknown reason, need more than 1 work item on CPU.

        :param string: str of symbols
        :param distance_increment: float > 0
        :param angle_increment: 0 <= float < 360
        :param angle_initial: 0 <= float < 360
        :param mapping: dict symbol: (str of symbols)
        :param margin: float >= 0
        :param kernel_filename: None or str
        :param device: None or OpenCL device
        :param nb_work_group: int >= 1
        :param nb_work_item: int >= 1
        :param debug: None or bool
        """
        assert isinstance(string, str), type(string)

        assert isinstance(distance_increment, float), type(distance_increment)
        assert distance_increment > 0, distance_increment

        assert isinstance(angle_increment, float), type(angle_increment)
        assert 0 <= angle_increment < 360, angle_increment

        assert isinstance(angle_initial, float), type(angle_initial)
        assert 0 <= angle_initial < 360, angle_initial

        assert isinstance(mapping, dict), type(mapping)

        assert isinstance(margin, float), type(margin)
        assert margin >= 0, margin

        assert isinstance(nb_work_group, int), type(nb_work_group)
        assert nb_work_group >= 1, nb_work_group

        assert isinstance(nb_work_item, int), type(nb_work_item)
        assert nb_work_item >= 1, nb_work_item

        assert (kernel_filename is None) or isinstance(kernel_filename, str), type(kernel_filename)

        assert (debug is None) or isinstance(debug, bool), type(debug)

        opencl_turtle.OpenCLTurtleBitmap.__init__(self, string, distance_increment,
                                                  angle_increment, angle_initial, mapping, margin,
                                                  kernel_filename=('opt_turtle.cl' if kernel_filename is None
                                                                   else kernel_filename),
                                                  device=device,
                                                  nb_work_group=nb_work_group, nb_work_item=nb_work_item,
                                                  debug=debug)
