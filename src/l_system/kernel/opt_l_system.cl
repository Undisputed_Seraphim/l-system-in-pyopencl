/* -*- coding: latin-1 -*- */
/**
 * Optimized OpenCL kernel for opt_opencl_l_system.OptOpenCLLSystem().
 *
 * Only one work group.
 * Number of work units must be at least equal than the number of production rules.
 *
 * GPLv3 --- Copyright (C) 2018 Olivier Pirson
 * Olivier Pirson --- http://www.opimedia.be/
 * August 17, 2018
 */

#include "assert.cl"



/* *****************
 * Macro constants *
 *******************/

#ifndef DEFAULT
# define DEFAULT 255  // value to mark absence of symbol
#endif

#ifndef G_SIZE
# define G_SIZE 1  // fake value, must be defined by host and must be equals to get_global_size(0)
#endif

#ifndef NB_RULE
# define NB_RULE 1  // fake value, must be defined by host
#endif



/* ************
 * Prototypes *
 **************/

/**
 * Compute the new value for r_max_size_to_expand.
 *
 * @param l_r_expanded_sizes
 *
 * @return new value for r_max_size_to_expand
 */
unsigned int
compute_r_expanded_max_size(__local unsigned int* l_r_expanded_sizes);


/**
 * Concatenate real symbols (not DEFAULT value) for each rule.
 * Each rule is concatenated by a work item.
 *
 * @param r_expanded_max_size != 0, maximal length to work for each rule
 * @param production_expanded buffer
 * @param r_array_size != 0, length of the buffer part for each rule
 * @param l_r_expanded_sizes array with the number of real symbols for each rule
 */
void
concatenate(const unsigned int r_expanded_max_size,
            __global unsigned char* const production_expanded,
            const unsigned int r_array_size,
            __local const unsigned int* const l_r_expanded_sizes
            ASSERT_DECLARE_PARAM);


/**
 * Expand each rule of production_to_expand with itself,
 * store result to production_expanded,
 * and return the number of real symbols (not DEFAULT) found by this work item..
 *
 * Each work unit will be expand a range of symbols.
 *
 * Result is not "concatenated".
 *
 * @parma production_to_expand
 * @parma r_max_size_to_expand
 * @parma production_expanded
 * @parma r_array_size length of the buffer part for each rule
 * @parma i_rule_to_expand
 *
 * @return number of real symbols found by this work item..
 */
unsigned int
expand_rule_double(__global unsigned char* const production_to_expand, const unsigned int r_max_size_to_expand,
                   __global unsigned char* const production_expanded,
                   const unsigned int r_array_size,
                   const unsigned int i_rule_to_expand
                   ASSERT_DECLARE_PARAM);


/**
 * Expand all rules of production_to_expand with themselves,
 * store result to production_expanded,
 * and return the number of real symbols (not DEFAULT) found by this work item..
 *
 * Each work unit will be expand a range of symbols.
 *
 * Result is not "concatenated".
 *
 * @parma production
 * @parma r_max_size
 * @parma production_to_expand
 * @parma r_max_size_to_expand
 * @parma production_expanded
 * @parma r_array_size length of the buffer part for each rule
 * @parma l_nb_reals
 * @parma l_r_expanded_sizes
 */
void
expand_rules_double(__global unsigned char* const production_to_expand, const unsigned int r_max_size_to_expand,
                    __global unsigned char* const production_expanded,
                    const unsigned int r_array_size,
                    __local unsigned int* const l_nb_reals,
                    __local unsigned int* const l_r_expanded_sizes
                    ASSERT_DECLARE_PARAM);


/**
 * Expand each rule of production_to_expand
 * with rules from production,
 * store result to production_expanded,
 * and return the number of real symbols (not DEFAULT) found by this work item..
 *
 * Each work unit will be expand a range of symbols.
 *
 * Result is not "concatenated".
 *
 * @parma production
 * @parma r_max_size
 * @parma production_to_expand
 * @parma r_max_size_to_expand
 * @parma production_expanded
 * @parma r_array_size length of the buffer part for each rule
 * @parma i_rule_to_expand
 *
 * @return number of real symbols found by this work item..
 */
unsigned int
expand_rule_once(__constant const unsigned char* const production, const unsigned int r_max_size,
                 __global unsigned char* const production_to_expand, const unsigned int r_max_size_to_expand,
                 __global unsigned char* const production_expanded,
                 const unsigned int r_array_size,
                 const unsigned int i_rule_to_expand
                 ASSERT_DECLARE_PARAM);


/**
 * Expand all rules of production_to_expand
 * with rules from production,
 * store result to production_expanded,
 * and return the number of real symbols (not DEFAULT) found by this work item..
 *
 * Each work unit will be expand a range of symbols.
 *
 * Result is not "concatenated".
 *
 * @parma production
 * @parma r_max_size
 * @parma production_to_expand
 * @parma r_max_size_to_expand
 * @parma production_expanded
 * @parma r_array_size length of the buffer part for each rule
 * @parma l_nb_reals
 * @parma l_r_expanded_sizes
 */
void
expand_rules_once(__constant const unsigned char* const production, const unsigned int r_max_size,
                  __global unsigned char* const production_to_expand, const unsigned int r_max_size_to_expand,
                  __global unsigned char* const production_expanded,
                  const unsigned int r_array_size,
                  __local unsigned int* const l_nb_reals,
                  __local unsigned int* const l_r_expanded_sizes
                  ASSERT_DECLARE_PARAM);


/**
 * Fill the first size items of array with the value symbol.
 * Each work unit fill a range of positions.
 */
void
fill(__global unsigned char* const array, const unsigned int size,
     const unsigned char symbol
     ASSERT_DECLARE_PARAM);


/**
 * Return n/d rounded to the smallest integer greater than or equal.
 *
 * @param n != 0
 * @param d != 0
 *
 * @return ceil(n/d)
 */
unsigned int
posint_ceil(unsigned int n, unsigned int d);



/* ***********
 * Functions *
 *************/

unsigned int
compute_r_expanded_max_size(__local unsigned int* l_r_expanded_sizes) {
  unsigned int r_expanded_max_size = l_r_expanded_sizes[0];

#pragma unroll
  for (unsigned int i = 1; i < NB_RULE; ++i) {
    r_expanded_max_size = max(r_expanded_max_size, l_r_expanded_sizes[i]);
  }

  return r_expanded_max_size;
}


void
concatenate(const unsigned int r_expanded_max_size,
            __global unsigned char* const production_expanded,
            const unsigned int r_array_size,
            __local const unsigned int* const l_r_expanded_sizes
            ASSERT_DECLARE_PARAM) {
  assert(r_expanded_max_size != 0);
  assert(r_array_size != 0);

  const unsigned int i_rule_to_expand = get_global_id(0);

  if (i_rule_to_expand < NB_RULE) {  // one work item for each rule
    unsigned int new_r_max_size_to_expand = l_r_expanded_sizes[i_rule_to_expand];
    __global unsigned char* dest_ptr = production_expanded + i_rule_to_expand*r_array_size;

    assert(new_r_max_size_to_expand != 0);
    assert(new_r_max_size_to_expand <= r_expanded_max_size);
    assert(*dest_ptr != DEFAULT);

    // Skip first real symbols in good position
    do {
      --new_r_max_size_to_expand;
      ++dest_ptr;
    } while ((*dest_ptr != DEFAULT) && new_r_max_size_to_expand);

    // Move left other real symbols
    for (__global unsigned char* src_ptr = dest_ptr; new_r_max_size_to_expand; ) {
      if (*(++src_ptr) != DEFAULT) {
        *(dest_ptr++) = *src_ptr;
        *src_ptr = DEFAULT;
        --new_r_max_size_to_expand;
      }
    }
  }
}


unsigned int
expand_rule_double(__global unsigned char* const production_to_expand, const unsigned int r_max_size_to_expand,
                   __global unsigned char* const production_expanded,
                   const unsigned int r_array_size,
                   const unsigned int i_rule_to_expand
                   ASSERT_DECLARE_PARAM) {
  assert0_v(r_array_size >= r_max_size_to_expand, r_array_size);

  const unsigned int g_id = get_global_id(0);
  const unsigned int range_size = posint_ceil(r_max_size_to_expand, G_SIZE);
  const unsigned int r_array_total_size = r_array_size*NB_RULE;

  const unsigned int offset_rule = i_rule_to_expand*r_array_size;
  const unsigned int i_symbol = g_id*range_size;
  const unsigned int offset_to_expand = offset_rule + i_symbol;
  const unsigned int offset_expanded = offset_rule + i_symbol*r_max_size_to_expand;

  unsigned int nb_real = 0;  // number of expanded real symbols found by this work unit

  for (unsigned int i = 0; i < range_size; ++i) {  // for each symbol of this range
    const unsigned int i_offset_to_expand = offset_to_expand + i;

    if (i_offset_to_expand < r_array_total_size) {
      const unsigned char symbol = production_to_expand[i_offset_to_expand];
      const unsigned int i_expanded = offset_expanded + i*r_max_size_to_expand;

      if (i_expanded + range_size < r_array_total_size) {
        unsigned int first_to_default = r_max_size_to_expand;

        // Copy the expansion of the current symbol of the current rule
        if (symbol < NB_RULE) {  // symbol is a variable, must be to expand
          for (unsigned int j = 0; j < r_max_size_to_expand; ++j) {
            const unsigned char symb = production_to_expand[symbol*r_array_size + j];

            if (symb != DEFAULT) {  // real symbol
              production_expanded[i_expanded + j] = symb;
              ++nb_real;
            }
            else {                  // symb is DEFAULT, so the remain for this symbol must be DEFAULT
              first_to_default = j;

              break;
            }
          }
        }
        else {                   // symbol is a constant (or DEFAULT)
          if (symbol != DEFAULT) {
            ++nb_real;
          }
          production_expanded[i_expanded] = symbol;
          first_to_default = 1;
        }

        for (__global unsigned char* ptr = production_expanded + i_expanded + first_to_default;
             first_to_default < r_max_size_to_expand; ++ptr, ++first_to_default) {
          *ptr = DEFAULT;
        }
      }
    }
  }

  assert0_v(nb_real <= r_max_size_to_expand*r_max_size_to_expand, nb_real);

  return nb_real;
}


void
expand_rules_double(__global unsigned char* const production_to_expand, const unsigned int r_max_size_to_expand,
                    __global unsigned char* const production_expanded,
                    const unsigned int r_array_size,
                    __local unsigned int* l_nb_reals,
                    __local unsigned int* l_r_expanded_sizes
                    ASSERT_DECLARE_PARAM) {
  assert_v(r_array_size >= r_max_size_to_expand, r_array_size);

  const unsigned int g_id = get_global_id(0);

  // Expand production_to_expand by apply to itself
#pragma unroll
  for (unsigned int i_rule_to_expand = 0; i_rule_to_expand < NB_RULE; ++i_rule_to_expand) {
    l_nb_reals[g_id] = expand_rule_double(production_to_expand, r_max_size_to_expand,
                                          production_expanded,
                                          r_array_size,
                                          i_rule_to_expand
                                          ASSERT_PARAM);

    barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);

    if (g_id == 0) {  // only the first work unit
      unsigned int nb_real = 0;

#pragma unroll
      for (unsigned int i = 0; i < G_SIZE; ++i) {  // collect for all work items for this rule
        nb_real += l_nb_reals[i];
      }

      l_r_expanded_sizes[i_rule_to_expand] = nb_real;
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    assert(l_r_expanded_sizes[i_rule_to_expand] != 0);
  }
}


unsigned int
expand_rule_once(__constant const unsigned char* const production, const unsigned int r_max_size,
                 __global unsigned char* const production_to_expand, const unsigned int r_max_size_to_expand,
                 __global unsigned char* const production_expanded,
                 const unsigned int r_array_size,
                 const unsigned int i_rule_to_expand
                 ASSERT_DECLARE_PARAM) {
  assert0(r_max_size != 0);
  assert0_v(r_max_size_to_expand >= r_max_size, r_max_size_to_expand);
  assert0_v(r_array_size >= r_max_size_to_expand, r_array_size);

  const unsigned int g_id = get_global_id(0);
  const unsigned int range_size = posint_ceil(r_max_size_to_expand, G_SIZE);
  const unsigned int r_array_total_size = r_array_size*NB_RULE;

  const unsigned int offset_rule = i_rule_to_expand*r_array_size;
  const unsigned int i_symbol = g_id*range_size;
  const unsigned int offset_to_expand = offset_rule + i_symbol;
  const unsigned int offset_expanded = offset_rule + i_symbol*r_max_size;

  unsigned int nb_real = 0;  // number of expanded real symbols found by this work unit

  for (unsigned int i = 0; i < range_size; ++i) {  // for each symbol of this range
    const unsigned int i_offset_to_expand = offset_to_expand + i;

    if (i_offset_to_expand < r_array_total_size) {
      const unsigned char symbol = production_to_expand[i_offset_to_expand];
      const unsigned int i_expanded = offset_expanded + i*r_max_size;

      if (i_expanded + range_size < r_array_total_size) {
        unsigned int first_to_default = r_max_size;

        // Copy the expansion of the current symbol of the current rule
        if (symbol < NB_RULE) {  // symbol is a variable, must be to expand
          for (unsigned int j = 0; j < r_max_size; ++j) {
            const unsigned char symb = production[symbol*r_max_size + j];

            if (symb != DEFAULT) {  // real symbol
              production_expanded[i_expanded + j] = symb;
              ++nb_real;
            }
            else {                  // symb is DEFAULT, so the remain for this symbol must be DEFAULT
              first_to_default = j;

              break;
            }
          }
        }
        else {                   // symbol is a constant (or DEFAULT)
          if (symbol != DEFAULT) {
            ++nb_real;
          }
          production_expanded[i_expanded] = symbol;
          first_to_default = 1;
        }

        for (__global unsigned char* ptr = production_expanded + i_expanded + first_to_default;
             first_to_default < r_max_size; ++ptr, ++first_to_default) {
          *ptr = DEFAULT;
        }
      }
    }
  }

  assert0_v(nb_real <= r_max_size_to_expand*r_max_size, nb_real);

  return nb_real;
}


void
expand_rules_once(__constant const unsigned char* const production, const unsigned int r_max_size,
                  __global unsigned char* const production_to_expand, const unsigned int r_max_size_to_expand,
                  __global unsigned char* const production_expanded,
                  const unsigned int r_array_size,
                  __local unsigned int* l_nb_reals,
                  __local unsigned int* l_r_expanded_sizes
                  ASSERT_DECLARE_PARAM) {
  assert(r_max_size != 0);
  assert_v(r_max_size_to_expand >= r_max_size, r_max_size_to_expand);
  assert_v(r_array_size >= r_max_size_to_expand, r_array_size);

  const unsigned int g_id = get_global_id(0);

  // Expand current rule to production_expanded
#pragma unroll
  for (unsigned int i_rule_to_expand = 0; i_rule_to_expand < NB_RULE; ++i_rule_to_expand) {
    l_nb_reals[g_id] = expand_rule_once(production, r_max_size,
                                        production_to_expand, r_max_size_to_expand,
                                        production_expanded,
                                        r_array_size,
                                        i_rule_to_expand
                                        ASSERT_PARAM);

    barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);

    if (g_id == 0) {  // only the first work unit
      unsigned int nb_real = 0;

#pragma unroll
      for (unsigned int i = 0; i < G_SIZE; ++i) {  // collect for all work items for this rule
        nb_real += l_nb_reals[i];
      }

      l_r_expanded_sizes[i_rule_to_expand] = nb_real;
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    assert(l_r_expanded_sizes[i_rule_to_expand] != 0);
  }
}


void
fill(__global unsigned char* const array, const unsigned int size,
     const unsigned char symbol
     ASSERT_DECLARE_PARAM) {
  const unsigned int g_id = get_global_id(0);
  const unsigned int tmp_range_size = posint_ceil(size, G_SIZE);
  const unsigned int offset = g_id*tmp_range_size;

  if (offset < size) {
    const unsigned int range_size = min(tmp_range_size,
                                        size - offset);  // for last ranges

    assert(offset + range_size <= size);

    for (unsigned int i = 0; i < range_size; ++i) {
      array[offset + i] = symbol;
    }
  }
}


unsigned int
posint_ceil(unsigned int n, unsigned int d) {
  return (n - 1)/d + 1;
}



/* ******
 * Main *
 ********/

/**
 * Kernel for opencl_l_system.OpenCLLSystem.production_iterate().
 *
 * Apply expand_rules_double() (with eventually expand_rules_once())
 * and concatenate() log_2(n) times.
 *
 * Swap production_to_expand and production_expanded to each iteration.
 * The final expansion is in production_expanded or in production_to_expand
 * depending to the value result[1].
 *
 * Return to result[0] the value of r_expanded_max_size
 *   corresponding to this final expansion
 * and 0 or 1 to result[1] to meaning that the final expansion
 *  is in production_expanded or in production_to_expand.
 *
 * @param nb_iteration >= 2
 * @param production
 * @param r_max_size >= 2
 * @param r_array_size
 * @param production_to_expand
 * @param production_expanded
 * @param result
 */
__kernel
void
fast_expand(unsigned int nb_iteration,
            __constant const unsigned char* const production, const unsigned int r_max_size,
            const unsigned int r_array_size,
            __global unsigned char* production_to_expand,
            __global unsigned char* production_expanded,
            __global unsigned int* const result
            ASSERT_DECLARE_PARAM) {
  assert_init;
  assert_v(G_SIZE >= 1, G_SIZE);
  assert_v(NB_RULE >= 1, NB_RULE);
  assert_v(G_SIZE >= NB_RULE, G_SIZE)

  assert_v(get_num_groups(0) == 1, get_num_groups(0));
  assert_v(get_global_size(0) == G_SIZE, get_global_size(0));
  assert_v(get_local_size(0) == G_SIZE, get_local_size(0));
  assert_v(get_global_id(0) == get_local_id(0), get_global_id(0));
  assert_v(nb_iteration >= 2, nb_iteration);
  assert_v(r_max_size >= 2, r_max_size);

  __local unsigned int l_nb_reals[G_SIZE];
  __local unsigned int l_r_expanded_sizes[NB_RULE];
  __local unsigned int l_r_expanded_max_size;

  // Init values of the result buffer
  fill(production_expanded, r_array_size*NB_RULE,
       DEFAULT
       ASSERT_PARAM);

  const unsigned int g_id = get_global_id(0);
  unsigned int r_max_size_to_expand = r_max_size;
  bool is_swapped = true;
  unsigned int mask = 1u << (30 - clz(nb_iteration));  // mask for binary figures of nb_iteration

  barrier(CLK_LOCAL_MEM_FENCE);


  while (mask > 0) {  // for log_2(nb_iteration) loops
    barrier(CLK_GLOBAL_MEM_FENCE);

    // Expand production_to_expand by apply to itself
    expand_rules_double(production_to_expand, r_max_size_to_expand,
                        production_expanded,
                        r_array_size,
                        l_nb_reals,
                        l_r_expanded_sizes
                        ASSERT_PARAM);


    // Compute new value for r_max_size_to_expand
    if (g_id == 0) {  // get max for all rules
      l_r_expanded_max_size = compute_r_expanded_max_size(l_r_expanded_sizes);
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    assert_v(l_r_expanded_max_size > r_max_size_to_expand, l_r_expanded_max_size);


    // Concatenate production_expanded
    assert(l_r_expanded_max_size != 0);

    concatenate(r_max_size_to_expand*r_max_size_to_expand,
                production_expanded,
                r_array_size,
                l_r_expanded_sizes
                ASSERT_PARAM);


    // Swap production_to_expand and production_expanded
    __global unsigned char* const tmp = production_to_expand;

    production_to_expand = production_expanded;
    production_expanded = tmp;

    is_swapped = !is_swapped;


    // Set new value
    r_max_size_to_expand = l_r_expanded_max_size;

    barrier(CLK_GLOBAL_MEM_FENCE);


    if (nb_iteration & mask) {  // apply one more time the initial production
      // Expand current rules to production_expanded
      expand_rules_once(production, r_max_size,
                        production_to_expand, r_max_size_to_expand,
                        production_expanded,
                        r_array_size,
                        l_nb_reals,
                        l_r_expanded_sizes
                        ASSERT_PARAM);


      // Compute new value for r_max_size_to_expand
      if (g_id == 0) {  // get max for all rules
        l_r_expanded_max_size = compute_r_expanded_max_size(l_r_expanded_sizes);
      }

      barrier(CLK_LOCAL_MEM_FENCE);

      assert_v(l_r_expanded_max_size > r_max_size_to_expand, l_r_expanded_max_size);


      // Concatenate production_expanded
      assert(l_r_expanded_max_size != 0);

      concatenate(r_max_size_to_expand*r_max_size,
                  production_expanded,
                  r_array_size,
                  l_r_expanded_sizes
                  ASSERT_PARAM);


      // Swap production_to_expand and production_expanded
      __global unsigned char* const tmp = production_to_expand;

      production_to_expand = production_expanded;
      production_expanded = tmp;

      is_swapped = !is_swapped;


      // Set new value
      r_max_size_to_expand = l_r_expanded_max_size;
    }

    mask >>= 1;
  }

  // Set result
  result[0] = l_r_expanded_max_size;
  result[1] = is_swapped;
}
