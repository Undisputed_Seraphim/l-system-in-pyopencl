/* -*- coding: latin-1 -*- */
/**
 * C-like assert alternative (because doesn't exist in OpenCL)
 * and other helper macros.
 *
 * GPLv3 --- Copyright (C) 2018 Olivier Pirson
 * Olivier Pirson --- http://www.opimedia.be/
 * August 15, 2018
 */

#undef println_su
#undef println_suu

#undef ASSERT_DECLARE_PARAM
#undef ASSERT_PARAM
#undef assert_init
#undef assert
#undef assert0
#undef assert_v
#undef assert0_v
#undef assert_just_return
#undef info
#undef last_info



#define println_su(message, value)
#define println_suu(message, value1, value2)


#ifdef NDEBUG
# define ASSERT_DECLARE_PARAM
# define ASSERT_PARAM
# define assert_init
# define assert(test)
# define assert0(test)
# define assert_v(test, value)
# define assert0_v(test, value)
# define assert_just_return(test)
# define info(value)
# define last_info(value)

#else
# define ASSERT_DECLARE_PARAM , __global unsigned int* const _assert_result
# define ASSERT_PARAM , _assert_result
# define assert_init {                          \
    _assert_result[0] = 0;                      \
    _assert_result[1] = 0;                      \
    barrier(CLK_GLOBAL_MEM_FENCE);              \
  }

# define assert(test) {                         \
    if (!(test)) {                              \
      if (_assert_result[0] == 0) {             \
        _assert_result[0] = __LINE__;           \
      }                                         \
                                                \
      return;                                   \
    }                                           \
  }

# define assert0(test) {                        \
    if (!(test)) {                              \
      if (_assert_result[0] == 0) {             \
        _assert_result[0] = __LINE__;           \
      }                                         \
                                                \
      return 0;                                 \
    }                                           \
  }

# define assert_v(test, value) {                \
    if (!(test)) {                              \
      if (_assert_result[0] == 0) {             \
        _assert_result[0] = __LINE__;           \
        _assert_result[1] = (value);            \
      }                                         \
                                                \
      return;                                   \
    }                                           \
  }

# define assert0_v(test, value) {               \
    if (!(test)) {                              \
      if (_assert_result[0] == 0) {             \
        _assert_result[0] = __LINE__;           \
        _assert_result[1] = (value);            \
      }                                         \
                                                \
      return 0;                                 \
    }                                           \
  }

# define assert_just_return(test) {             \
    if (!(test)) {                              \
      return;                                   \
    }                                           \
  }

# define info(value) {                          \
    if (_assert_result[1] == 0) {               \
      _assert_result[1] = (value);              \
    }                                           \
  }

# define last_info(value) _assert_result[1] = (value)

# if __OPENCL_C_VERSION__ >= CL_VERSION_2_0  // for my clang compiler
#   ifdef CL_VERSION_2_0  // for my PyOpenCL version
#     undef println_su
#     undef println_suu
#     define println_su(message, value) printf("%s %u\n", message, value)
#     define println_suu(message, value1, value2) printf("%s %u %u\n", message, value1, value2)
#   endif
# endif
#endif
