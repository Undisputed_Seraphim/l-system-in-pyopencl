#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Create the value for the convert array of opt_turtle.cl.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 18, 2018
"""

from __future__ import division
from __future__ import print_function


########
# Main #
########
def main():
    d = {'F': '0,  /* F */ \\\n',
         'f': '1,  /* f */ \\\n',
         '+': '2,  /* + */ \\\n',
         '-': '3,  /* - */ \\\n'}

    l = []
    for i in range(255):
        l.append(d.get(chr(i), '4, '))

    print(''.join(l))


if __name__ == '__main__':
    main()
