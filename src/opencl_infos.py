#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""
Print information on all OpenCL platforms available and all its devices.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 17, 2018
"""

from __future__ import division
from __future__ import print_function

import sys

import pyopencl as cl

if sys.version_info[0] >= 3:
    long = int


def help_quit(error):
    """
    Print the help message on stderr
    and exit with the error code.

    :param error: 0 <= int < 256
    """
    assert isinstance(error, int), type(error)
    assert 0 <= error < 256, error

    print("""Usage: opencl_infos.py [--complete]
Print information on all OpenCL platforms available and all its devices.

Option:
  --complete   all information about device, else only main information""",
          file=sys.stderr)
    exit(error)


def print_device(device, complete):
    """
    Print information about the device.

    :param device: pyopencl.Device
    :param complete: bool
    """
    assert isinstance(device, cl.Device), type(device)
    assert isinstance(complete, bool), type(complete)

    print('  Device name: "{}"'.format(device.name))

    attributes = (tuple(sorted(dir(device))) if complete
                  else ('global_mem_cache_size', 'global_mem_size',
                        'local_mem_size',
                        'max_clock_frequency',
                        'max_compute_units',
                        'max_constant_buffer_size',
                        'max_mem_alloc_size',
                        'max_work_group_size', 'max_work_item_sizes',
                        'opencl_c_version'))
    for attribute in attributes:
        if (attribute[:1] != '_') and (attribute != 'name'):
            try:
                value = getattr(device, attribute)
            except:
                continue

            attribute = attribute.replace('_', ' ')
            if isinstance(value, int) or isinstance(value, long):
                print('         {}: {}'
                      .format(attribute,
                              (size_units(value) if attribute[-4:] == 'size'
                               else value)))
            elif isinstance(value, list) or isinstance(value, tuple):
                print('         {}: {}'
                      .format(attribute, ' '.join(str(v) for v in value)))
            elif isinstance(value, str):
                print('         {}: "{}"'.format(attribute, value))


def print_platform(platform, platform_id, complete):
    """
    Print information about the platform and its devices.

    :param platform: pyopencl.Platform
    :param platform_id: int >= 0
    :param complete: bool
    """
    assert isinstance(platform, cl.Platform), type(platform)

    assert isinstance(platform_id, int), type(platform_id)
    assert platform_id >= 0, platform_id

    assert isinstance(complete, bool), type(complete)

    print('Platform name: "{}"'.format(platform.name))
    print('         vendor: "{}"'.format(platform.vendor))
    print('         version: "{}"'.format(platform.version))
    print('         profile: "{}"'.format(platform.profile))
    print('         extensions: "{}"'.format(platform.extensions))

    devices = platform.get_devices()
    nb_device = len(devices)
    print('  {} device{}'.format(nb_device, s(nb_device)))

    for device_id, device in enumerate(devices):
        print(' ', '{}:{}'.format(platform_id, device_id), '-' * 30)
        print_device(device, complete)


def s(x):
    """
    If x >= 2 then return 's',
    else return ''.

    :param x: int or long or float

    :return: 's' or ''
    """
    return ('s' if x >= 2
            else '')


def size_units(n):
    """
    Return a string with the size n expressed in several units.

    :param: (int or long) >= 0

    :return: str
    """
    assert isinstance(n, int) or isinstance(n, long), type(n)
    assert n >= 0, n

    seq = ['{} o'.format(n)]
    units = 'kMGT'

    while True:
        n //= 1024
        unit, units = units[0], units[1:]
        if n <= 0:
            break

        seq.append('{} {}io'.format(n, unit))

    return ' = '.join(seq)


########
# Main #
########
def main():
    """
    Print information on all OpenCL platforms
    and its devices.
    """
    complete = False
    for arg in sys.argv[1:]:
        if arg == '--complete':
            complete = True
        elif arg == '--help':
            help_quit(0)
        else:
            help_quit(1)

    print('Python version:', sys.version.replace('\n', ' '))
    print('PyOpenCL version:', cl.VERSION_TEXT)

    platforms = cl.get_platforms()

    nb_platform = len(platforms)
    print('{} OpenCL platform{}'.format(nb_platform, s(nb_platform)))

    for platform_id, platform in enumerate(platforms):
        print('=' * 40)
        print_platform(platform, platform_id, complete)

if __name__ == '__main__':
    main()
