#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Benchmarks implementations of L-system expansion.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 18, 2018
"""

from __future__ import division
from __future__ import print_function

import gc
import sys
import time

import l_system.simple_l_system
import l_system.fast_l_system
import l_system.array_l_system
import l_system.opencl_l_system
import l_system.opt_opencl_l_system


def f(x):
    """
    :param x: float

    :return: str
    """
    assert isinstance(x, float), type(x)

    return '{:.6f}'.format(x)


def fs(seq):
    """
    :param seq: list of float

    :return: str
    """
    assert isinstance(seq, list), type(seq)

    return '[' + ', '.join([f(x) for x in seq]) + ']'


def li(x):
    """
    :param x: int or long

    :return: str
    """
    return str(x).replace('L', '')


def lis(seq):
    """
    :param seq: list of (int or long)

    :return: str
    """
    assert isinstance(seq, list), type(seq)

    return '[' + ', '.join([li(x) for x in seq]) + ']'


########
# Main #
########
def main():
    """
    Benchmarks implementations of L-system expansion.
    """
    filename = '../systems/dragon_curve.txt'
    implementations = (l_system.simple_l_system.LSystem,
                       l_system.fast_l_system.FastLSystem,
                       # l_system.array_l_system.ArrayLSystem,
                       # l_system.array_l_system.FastArrayLSystem,
                       l_system.opencl_l_system.OpenCLLSystem,
                       l_system.opencl_l_system.FastOpenCLLSystem,
                       l_system.opt_opencl_l_system.OptFastOpenCLLSystem)
    nb_skip = 3
    nb_repetition = 10

    name, alphabet, axiom, production = l_system.simple_l_system.load_l_system(filename)

    python_version = 'Python version: {}{}'.format(sys.version.replace('\n', ' '),
                                                   (' DEBUG MODE' if __debug__
                                                    else ''))
    print('# iteration\tlength\timplementation\tdevice\tGPU?\t# work group\t# work item\ttotal time\ttotal opencl\t# correct\taverage\taverage opencl\ttimes\ttimes opencl',
          name, nb_skip, nb_repetition, python_version,
          sep='\t')
    print('# iteration\tlength\timplementation\tdevice\tGPU?\t# work group\t# work item\trepetition\tsucceed?\terror',
          name, nb_skip, nb_repetition, python_version,
          sep='\t', file=sys.stderr)

    for nb_iteration in range(21):  # for each number of iterations
        lsystem2 = l_system.fast_l_system.FastLSystem(alphabet, axiom, production, name)
        lsystem2.expand(nb_iteration)
        correct_string = str(lsystem2)
        len_correct_string = len(correct_string)

        del lsystem2
        gc.collect()

        for ImplLSystem in implementations:  # for each implementation
            is_opencl = ('OpenCL' in ImplLSystem.__name__)
            for platform_id in range(2 if is_opencl else 1):  # just 1 or for each OpenCL platform
                if is_opencl:
                    device = l_system.opencl_l_system.get_device(platform_id, 0)
                    gpu = ('GPU' if l_system.opencl_l_system.device_is_nvidia(device) else 'CPU')
                else:
                    gpu = 'CPU'

                for k in range(11 if is_opencl else 1):  # for 1 process or several nb of work items
                    nb_work_item = 2**k  # 1 or 1, 2, 4, 8, 16 ... 1024
                    if is_opencl and not l_system.opencl_l_system.device_is_ok(device, 1, nb_work_item):
                        break
                    if is_opencl and (nb_work_item < len(production)):
                        continue

                    lsystem = None
                    print(nb_iteration, len_correct_string,
                          ImplLSystem.__name__,
                          platform_id, gpu, 1, nb_work_item, sep='\t', end='\t')
                    sys.stdout.flush()

                    for i in range(nb_skip + nb_repetition):  # for each iteration
                        is_skip = (i < nb_skip)
                        if i == nb_skip:
                            durations_s = []
                            cl_durations_ns = []

                        duration_s = 0.0
                        lsystem = None

                        start = time.time()
                        try:
                            lsystem = (ImplLSystem(alphabet, axiom, production, name,
                                                   device=device, nb_work_item=nb_work_item)
                                       if is_opencl
                                       else ImplLSystem(alphabet, axiom, production, name))
                            lsystem.expand(nb_iteration)

                        except Exception as ex:
                            print(nb_iteration, len_correct_string,
                                  ImplLSystem.__name__, platform_id, gpu, 1, nb_work_item, i,
                                  'FAILED!', ex,
                                  sep='\t', file=sys.stderr)
                            sys.stderr.flush()

                        end = time.time()
                        is_correct = (str(lsystem) == correct_string)
                        if is_correct:
                            print(nb_iteration, len_correct_string,
                                  ImplLSystem.__name__, platform_id, gpu, 1, nb_work_item, i,
                                  ('skipped' if is_skip else 'succeed'),
                                  sep='\t', file=sys.stderr)
                            sys.stderr.flush()
                        else:
                            print(nb_iteration, len_correct_string,
                                  ImplLSystem.__name__, platform_id, gpu, 1, nb_work_item, i,
                                  ('INCORRECT!'),
                                  sep='\t', file=sys.stderr)
                            sys.stderr.flush()
                        if not is_skip:
                            if is_correct:
                                duration_s = end - start
                                durations_s.append(duration_s)
                                if is_opencl:
                                    cl_duration_ns = lsystem.cl_duration_ns()
                                    cl_durations_ns.append(cl_duration_ns)
                                else:
                                    cl_durations_ns.append(0)

                    del lsystem

                    nb = len(durations_s)
                    total_duration_s = float(sum(durations_s))
                    average_s = (total_duration_s / nb if nb != 0 else 0.0)
                    if is_opencl:
                        cl_total_duration_ns = sum(cl_durations_ns)
                        cl_average_ns = (float(cl_total_duration_ns) / nb if nb != 0 else 0.0)
                    else:
                        cl_total_duration_ns = 0
                        cl_average_ns = 0.0
                    print(f(total_duration_s), li(cl_total_duration_ns),
                          nb,
                          f(average_s), f(cl_average_ns),
                          fs(durations_s), lis(cl_durations_ns), sep='\t')
                    sys.stdout.flush()


if __name__ == '__main__':
    main()
