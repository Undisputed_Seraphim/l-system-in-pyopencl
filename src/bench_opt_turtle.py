#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Simple benchmarks of OptOpenCLTurtleBitmap
use during development.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 18, 2018
"""

from __future__ import division
from __future__ import print_function

import gc
import sys
import time

import numpy as np

import l_system.simple_l_system
import l_system.fast_l_system
import l_system.opencl_l_system
import l_system.turtle
import l_system.opencl_turtle
import l_system.opt_opencl_turtle


########
# Main #
########
def main():
    """
    Run OptOpenCLTurtleBitmap with several number of work items.
    """
    filename = '../systems/dragon_curve.txt'
    mapping_filename = '../systems/turtle/turtle_dragon_curve.txt'
    nb_iteration = 19

    try:
        nb_iteration = int(sys.argv[1])
    except:
        pass

    name, alphabet, axiom, production = l_system.simple_l_system.load_l_system(filename)
    angle_increment, mapping = l_system.turtle.load_turtle_mapping(mapping_filename)

    device = l_system.opencl_l_system.get_device(0, 0)

    # Compute with FastLSystem to compare result
    lsystem2 = l_system.fast_l_system.FastLSystem(alphabet, axiom, production, name)
    lsystem2.expand(nb_iteration)
    string = str(lsystem2)
    turt2 = l_system.opencl_turtle.OpenCLTurtleBitmap(string, 8.0,
                                                      angle_increment, 90.0,
                                                      mapping,
                                                      5.0,
                                                      device=device,
                                                      debug=False)
    sys.stdout.flush()
    turt2.process()
    correct_canvas = turt2.canvas()
    # turt2.save_image('bench_opt_turtle2.png')
    sys.stdout.flush()

    del lsystem2
    del turt2

    print(name, nb_iteration, correct_canvas.shape,
          'Python version: {}{}'.format(sys.version.replace('\n', ' '),
                                        (' DEBUG MODE' if __debug__
                                         else '')),
          sep='\t')

    ImplTurtle = l_system.opt_opencl_turtle.OptOpenCLTurtleBitmap

    total_cl_duration = 0

    for nb_work_group in (2, 8, 256):
        for nb_work_item in (2, 4, 16, 256):
            turt = None
            print(ImplTurtle.__name__,
                  nb_work_group, nb_work_item, sep='\t', end='\t')
            sys.stdout.flush()

            try:
                start = time.time()

                turt = ImplTurtle(string, 8.0,
                                  angle_increment, 90.0,
                                  mapping,
                                  5.0,
                                  device=device, nb_work_group=nb_work_group, nb_work_item=nb_work_item,
                                  debug=False)
                turt.process()

                end = time.time()
                duration = '{:.3f}'.format(end - start)
                correct = ('v' if np.array_equal(turt.canvas(), correct_canvas)
                           else 'x')
            except Exception as ex:
                print('FAILED!', ex, file=sys.stderr)
                duration = 'x'
                correct = 'x'

            print(duration, correct,
                  '{:.3f}'.format(turt.cl_duration_s(), sep='\t', end=''))
            sys.stdout.flush()
            total_cl_duration += turt.cl_duration_s()

            # turt.save_image('bench_opt_turtle.png')

            del turt
            gc.collect()

        print()

    print('Total: {:.3f}'.format(total_cl_duration))

if __name__ == '__main__':
    main()
