#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
Simple benchmarks of OptFastOpenCLLSystem
use during development.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 18, 2018
"""

from __future__ import division
from __future__ import print_function

import gc
import sys
import time

import l_system.simple_l_system
import l_system.fast_l_system
import l_system.opencl_l_system
import l_system.opt_opencl_l_system


########
# Main #
########
def main():
    """
    Run OptFastOpenCLLSystem with several number of work items.
    """
    filename = '../systems/dragon_curve.txt'
    nb_iteration = 17

    try:
        nb_iteration = int(sys.argv[1])
    except:
        pass

    name, alphabet, axiom, production = l_system.simple_l_system.load_l_system(filename)

    # Compute with FastLSystem to compare result
    lsystem2 = l_system.fast_l_system.FastLSystem(alphabet, axiom, production, name)
    lsystem2.expand(nb_iteration)
    correct_string = str(lsystem2)

    del lsystem2

    print(name, nb_iteration, len(correct_string),
          'Python version: {}{}'.format(sys.version.replace('\n', ' '),
                                        (' DEBUG MODE' if __debug__
                                         else '')),
          sep='\t')

    device = l_system.opencl_l_system.get_device(0, 0)
    ImplLSystem = l_system.opt_opencl_l_system.OptFastOpenCLLSystem

    total_cl_duration = 0

    for nb_work_item in (2, 4, 16, 256):
        lsystem = None
        print(ImplLSystem.__name__,
              nb_work_item, sep='\t', end='\t')
        sys.stdout.flush()

        try:
            start = time.time()

            lsystem = ImplLSystem(alphabet, axiom, production, name,
                                  device=device, nb_work_item=nb_work_item,
                                  debug=False)
            lsystem.expand(nb_iteration)

            end = time.time()
            duration = '{:.3f}'.format(end - start)
            correct = ('v' if str(lsystem) == correct_string
                       else 'x')
        except Exception as ex:
            print('FAILED!', ex, file=sys.stderr)
            duration = 'x'
            correct = 'x'

        print(duration, correct,
              '{:.3f}'.format(lsystem.cl_duration_s(), sep='\t', end=''))
        sys.stdout.flush()
        total_cl_duration += lsystem.cl_duration_s()

        del lsystem
        gc.collect()

    print('Total: {:.3f}'.format(total_cl_duration))

if __name__ == '__main__':
    main()
