#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
L-system (Lindenmayer system) generator.

See https://en.wikipedia.org/wiki/L-system

To see options:
``$ ./prog_l_system.py --help``

By example:
``./prog_l_system.py ../systems/dragon_curve.txt``

Or, to run with multiple assertions disabled:
``python -OO ./prog_l_system.py ../systems/dragon_curve.txt``

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 18, 2018
"""

from __future__ import division
from __future__ import print_function

import os
import sys
import time

import l_system
import l_system.simple_l_system
import l_system.fast_l_system
import l_system.turtle

try:
    import l_system.array_l_system
except ImportError:
    print('Module l_system.array_l_system NOT available, probably because NumPy not available!',
          file=sys.stderr)

try:
    import l_system.opencl_l_system
    import l_system.opt_opencl_l_system
    import l_system.opencl_turtle
    import l_system.opt_opencl_turtle
except ImportError:
    print('Module l_system.opencl_l_system NOT available, probably because PyOpenCL not available!',
          file=sys.stderr)


def help_quit(error):
    """
    Print the help message on stderr
    and exit with the error code.

    :param error: 0 <= int < 256
    """
    assert isinstance(error, int), type(error)
    assert 0 <= error < 256, error

    print("""Usage: prog_l_system.py [OPTION]... [FILE]
Load a FILE that describe a L-system (or dragon curve by default)
and compute the expansion.
If an image filename is specified,
then produce a picture representation of the final result.

Options:
  --nb-step N          number of results computed and printed from the axiom (default 3)
  --size-step N        number of iterations by step (default 1)

  --angle X            initial direction for the picture (default 0.0)
  --distance X         distance of a move for the picture (default 5.0)
  --image FILE         filename to save the picture (extension .eps (only not OpenCL), .png or .jpg)
  --mapping FILE       filename to load the turtle mapping (mandatory for most of L-system)

  --fast               used the faster Python implementation FastLSystem instead LSystem
  --array              used the Python implementation ArrayLSystem instead LSystem
  --fastarray          used the Python implementation FastArrayLSystem instead LSystem
  --opencl P:D         used the OpenCL implementation OpenCLSystem instead LSystem
                       with platform number P and device number D
  --fastopencl P:D     used the OpenCL implementation FastOpenCLSystem instead LSystem
  --optfastopencl P:D  used the OpenCL implementation OptFastOpenCLSystem instead LSystem

  --opencl-debug       enable debug mode in OpenCL (default is debug mode or not from Python)
  --opencl-ndebug      disable debug mode in OpenCL
  --work-group N       number of work groups used by OpenCL implementation to build image (default 2)
  --work-item N        number of work items used by OpenCL implementation (default 4)
                       (must be at least equal than the number of production rules)

  --check              check the final result by comparing the result with FastLSystem (or LSystem)
  --no-expand          print only the information, not compute the expansion
  --no-result          print without the result string of the expansion

  --help               print this help message and quit
  --quiet              print only the results of expansion""", file=sys.stderr)
    exit(error)


def print_result(iteration, result, is_verbose, is_print_result):
    """
    If is_verbose and is_result then print "iteration: size result",
    if not is_verbose and is_result then print only "result",
    if is_verbose and not is_result then print only "iteration: size result",
    else do nothing.

    :param iteration: int >= 0
    :param result: str
    :param is_verbose: bool
    :param is_print_result: bool
    """
    assert isinstance(iteration, int), type(iteration)
    assert iteration >= 0, iteration

    assert isinstance(result, str), type(result)
    assert isinstance(is_verbose, bool), type(is_verbose)

    if is_verbose:
        print('{:3}: {:9} '.format(iteration, len(result)), end='')
    if is_print_result:
        print(result)
    elif is_verbose:
        print()


########
# Main #
########
def main():
    """
    Get parameters from the command line,
    compute and print the L-system,
    and maybe save the corresponding picture.
    """
    # Default parameters
    is_bitmap = False
    is_check = False
    is_expand = True
    is_print_result = True
    is_verbose = True
    filename = None
    image_filename = None
    mapping_filename = None
    nb_step = 3
    size_step = 1
    distance_increment = 5.0
    angle_initial = 0.0
    ImplLSystem = l_system.simple_l_system.LSystem
    ImplTurtle = None
    opencl_platform_id = 0
    opencl_device_id = 0
    opencl_nb_work_group = 2
    opencl_nb_work_item = 4
    opencl_debug_mode = None

    # Read parameters
    i = 1
    while i < len(sys.argv):
        arg = sys.argv[i]
        number = None
        if arg[:2] == '--':
            try:
                if arg == '--angle':
                    i += 1
                    number = sys.argv[i]
                    angle_initial = float(number) % 360
                elif arg == '--array':
                    ImplLSystem = l_system.array_l_system.ArrayLSystem
                elif arg == '--check':
                    is_check = True
                elif arg == '--distance':
                    i += 1
                    number = sys.argv[i]
                    distance_increment = float(number)
                    if float(number) <= 0:
                        print('Wrong value "{}"!'.format(number),
                              file=sys.stderr)

                        help_quit(1)
                elif arg == '--fast':
                    ImplLSystem = l_system.fast_l_system.FastLSystem
                elif arg == '--fastarray':
                    ImplLSystem = l_system.array_l_system.FastArrayLSystem
                elif arg == '--fastopencl':
                    ImplLSystem = l_system.opencl_l_system.FastOpenCLLSystem
                    i += 1
                    pieces = sys.argv[i].split(':')
                    opencl_platform_id = int(pieces[0])
                    opencl_device_id = int(pieces[1])
                elif arg == '--help':
                    help_quit(0)
                elif arg == '--image':
                    i += 1
                    image_filename = sys.argv[i]
                    ext = image_filename[-4:].lower()
                    is_bitmap = (ext != '.eps')
                    if ext not in ('.eps', '.jpg', '.png'):
                        print('Wrong extension "{}"!'.format(image_filename),
                              file=sys.stderr)

                        help_quit(1)
                elif arg == '--mapping':
                    i += 1
                    mapping_filename = sys.argv[i]
                elif arg == '--nb-step':
                    i += 1
                    number = sys.argv[i]
                    nb_step = int(number)
                elif arg == '--no-expand':
                    is_expand = False
                elif arg == '--no-result':
                    is_print_result = False
                elif arg == '--opencl':
                    ImplLSystem = l_system.opencl_l_system.OpenCLLSystem
                    i += 1
                    pieces = sys.argv[i].split(':')
                    opencl_platform_id = int(pieces[0])
                    opencl_device_id = int(pieces[1])
                elif arg == '--opencl-debug':
                    opencl_debug_mode = True
                elif arg == '--opencl-ndebug':
                    opencl_debug_mode = False
                elif arg == '--optfastopencl':
                    ImplLSystem = l_system.opt_opencl_l_system.OptFastOpenCLLSystem
                    i += 1
                    pieces = sys.argv[i].split(':')
                    opencl_platform_id = int(pieces[0])
                    opencl_device_id = int(pieces[1])
                elif arg == '--size-step':
                    i += 1
                    number = sys.argv[i]
                    size_step = int(number)
                elif arg == '--quiet':
                    is_verbose = False
                elif arg == '--work-group':
                    i += 1
                    number = sys.argv[i]
                    opencl_nb_work_group = int(number)
                    if int(number) <= 0:
                        print('Wrong value "{}"!'.format(number),
                              file=sys.stderr)

                        help_quit(1)
                elif arg == '--work-item':
                    i += 1
                    number = sys.argv[i]
                    opencl_nb_work_item = int(number)
                    if int(number) <= 0:
                        print('Wrong value "{}"!'.format(number),
                              file=sys.stderr)

                        help_quit(1)
                else:
                    print('Unknown parameter "{}"!'.format(arg),
                          file=sys.stderr)

                    help_quit(1)

                if (number is not None) and (float(number) < 0):
                    print('Wrong value "{}"!'.format(number), file=sys.stderr)

                    help_quit(1)
            except IndexError:
                print('Missing parameter!', file=sys.stderr)

                help_quit(1)
            except ValueError:
                print('Wrong value "{}"!'.format(number), file=sys.stderr)

                help_quit(1)
        else:
            filename = arg
        i += 1

    is_opencl = ('OpenCL' in ImplLSystem.__name__)
    is_opt_opencl = is_opencl and ('Opt' in ImplLSystem.__name__)

    if image_filename is not None:
        if is_opencl:
            if not is_bitmap:
                print('PostScript picture NOT available with OpenCL!',
                      file=sys.stderr)

                help_quit(1)
            ImplTurtle = (l_system.opt_opencl_turtle.OptOpenCLTurtleBitmap if is_opt_opencl
                          else l_system.opencl_turtle.OpenCLTurtleBitmap)
        else:
            ImplTurtle = (l_system.turtle.TurtleBitmap if is_bitmap
                          else l_system.turtle.TurtlePostscript)

    # Load L-system or dragon curve by default
    if (filename is not None) and not os.path.isfile(filename):
        print('Can\'t read "{}"!'.format(filename), file=sys.stderr)

        help_quit(1)

    if filename is None:
        name = 'dragon curve'
        alphabet = 'XYF+-'
        axiom = 'FX'
        production = {'X': 'X+YF+',
                      'Y': '-FX-Y'}
    else:
        name, alphabet, axiom, production = l_system.simple_l_system.load_l_system(filename)

    # Print parameters
    if is_verbose:
        print('Python version:', sys.version.replace('\n', ' '))
        print('l_system version: {} --- Implementation used: {}'
              .format(l_system.VERSION, ImplLSystem.__name__), end='')
        if image_filename is not None:
            print(' and', ImplTurtle.__name__, end='')
        print()
        print('Name:', name)
        print('Alphabet:', alphabet)
        print('Axiom:', axiom)
        print('Production rules:')
        for symbol, result in sorted(production.items()):
            print('  {}: {}'.format(symbol, result))
        print('Number of steps:', nb_step)
        print('Size of each step:', size_step)

        r_max_nb_variable = l_system.simple_l_system.rule_max_nb_variable(production)
        r_max_nb_constant = l_system.simple_l_system.rule_max_nb_constant(alphabet, production)
        r_max_size = l_system.simple_l_system.rule_max_size(production)

        variable, constant = l_system.simple_l_system.split_string(axiom, alphabet, production)
        s_nb_variable = len(variable)
        s_nb_constant = len(constant)
        s_size = len(axiom)

        assert s_nb_variable + s_nb_constant == s_size

        print('Rule maximum number of variables:', r_max_nb_variable)
        print('Rule maximum number of constants:', r_max_nb_constant)
        print('Rule maximum size:', r_max_size)
        print('Axiom number of variables:', s_nb_variable)
        print('Axiom number of constants:', s_nb_constant)
        print('Axiom size:', s_size)
        if is_opencl:
            opencl_device = l_system.opencl_l_system.get_device(opencl_platform_id, opencl_device_id)
            print("""OpenCL device: {}:{} {}
Number of work groups: {}
Number of work items: {}""".format(opencl_platform_id, opencl_device_id, opencl_device,
                                   opencl_nb_work_group, opencl_nb_work_item))
            if not l_system.opencl_l_system.device_is_ok(opencl_device,
                                                         opencl_nb_work_group, opencl_nb_work_item):
                        print('\nImpossible number of work groups "{}" and/or number of work items "{}" !'
                              .format(opencl_nb_work_group, opencl_nb_work_item), file=sys.stderr)

                        help_quit(1)

        print("""Upper bounds for expansion of one production rule ...:
#it:     #var. +   #const. =      size (factor) <= (rule maximum size)^#it
                                 ... and for expansion of the axiom ---     #var. +   #const. =      size (factor) <= (axiom size)*(rule maximum size)^#it""")

        prev_r_bound_size = None
        prev_s_bound_size = None
        for num_step in range(nb_step + 1):
            nb_iteration = size_step * num_step

            r_bound_variable, r_bound_constant, r_bound_size \
                = l_system.simple_l_system.rule_iter_upper_bounds(nb_iteration,
                                                                  r_max_nb_variable, r_max_nb_constant,
                                                                  r_max_size)

            s_bound_variable, s_bound_constant, s_bound_size \
                = l_system.simple_l_system.string_iter_upper_bounds(nb_iteration,
                                                                    r_max_nb_variable, r_max_nb_constant,
                                                                    r_max_size,
                                                                    s_nb_variable, s_nb_constant,
                                                                    s_size)

            def factor(value, prev):
                """
                Return a string representation of value/prev.

                :param value: None or int >= 1
                :param prev: int >= 1

                :return: str
                """
                assert isinstance(value, int), type(value)
                assert value >= 1, value

                assert (prev is None) or isinstance(prev, int), type(prev)
                assert (prev is None) or prev >= 1, prev

                if prev is None:
                    return ' ' * 8
                elif value % prev == 0:
                    return '({:2}    )'.format(value // prev)
                else:
                    return '({:6.3f})'.format(float(value) / prev)

            print('{:3}: {:9} + {:9} = {:9} {} <= {:16} --- {:9} + {:9} = {:9} {} <= {:16}'
                  .format(nb_iteration,
                          r_bound_variable, r_bound_constant, r_bound_size,
                          factor(r_bound_size, prev_r_bound_size),
                          r_max_size**nb_iteration,
                          s_bound_variable, s_bound_constant, s_bound_size,
                          factor(s_bound_size, prev_s_bound_size),
                          s_size * (r_max_size**nb_iteration)))

            prev_r_bound_size = r_bound_size
            prev_s_bound_size = s_bound_size

    if is_expand:
        if opencl_nb_work_item < len(production):
            print('\nWrong value {} < {}!'.format(opencl_nb_work_item, len(production)),
                  file=sys.stderr)

            help_quit(1)

        # Init the L-system
        def build(production):
            """
            Return the good instance built with the given production rules.
            """
            return (ImplLSystem(alphabet, axiom, production, name,
                                device=opencl_device, nb_work_item=opencl_nb_work_item,
                                debug=opencl_debug_mode)
                    if is_opencl
                    else ImplLSystem(alphabet, axiom, production, name))

        lsystem = build(production)

        # Compute and print
        if is_verbose:
            print("""Expansion of the axiom:
#it:      size{}""".format(' result' if is_print_result else ''))

        # Compute the size_step iterations equivalent production rules
        start = time.time()
        production = lsystem.production_iterate(size_step)
        lsystem = build(production)
        duration_s = time.time() - start

        # Compute the nb_step steps with theses new production rules
        print_result(lsystem.iteration(), str(lsystem), is_verbose, is_print_result)
        for _ in range(nb_step):
            start = time.time()
            lsystem.expand()
            duration_s += time.time() - start
            print_result(lsystem.iteration(), str(lsystem), is_verbose, is_print_result)

        if is_verbose:
            print('Calculation time: {:.3f} s'.format(duration_s))

        if is_check:
            lsystem2 = (l_system.simple_l_system.LSystem(alphabet, axiom, production, name)
                        if lsystem.__class__.__name__ == 'FastLSystem'
                        else l_system.fast_l_system.FastLSystem(alphabet, axiom, production, name))
            lsystem2.expand(nb_iteration)
            result2 = str(lsystem2)
            if str(lsystem) == result2:
                print('Check: Comparison of iteration {} with {} succeed.'
                      .format(nb_iteration, lsystem2.__class__.__name__))
            else:
                print('Check: Comparison of iteration {} with {} FAILED! {}'
                      .format(nb_iteration, lsystem2.__class__.__name__, result2))

        # Build image
        if image_filename is not None:
            angle_increment, mapping = (l_system.turtle.load_turtle_mapping(mapping_filename)
                                        if mapping_filename is not None
                                        else (0.0, dict()))
            if is_verbose:
                print('Angle increment:', angle_increment)
                print('Turtle mapping:')
                for symbol, mapped in sorted(mapping.items()):
                    print('  {}: {}'.format(symbol, mapped))

            if is_opencl:
                turt = ImplTurtle(str(lsystem), distance_increment,  # OpenCLTurtleBitmap
                                  angle_increment, angle_initial,    # or OptOpenCLTurtleBitmap
                                  mapping,
                                  5.0,
                                  device=opencl_device,
                                  nb_work_group=opencl_nb_work_group,
                                  nb_work_item=opencl_nb_work_item,
                                  debug=opencl_debug_mode)
            else:
                turt = (ImplTurtle(str(lsystem), distance_increment,  # TurtleBitmap
                                   angle_increment, angle_initial,
                                   mapping,
                                   5.0)
                        if is_bitmap
                        else ImplTurtle(str(lsystem), distance_increment,  # TurtlePostcript
                                        angle_increment, angle_initial,
                                        mapping,
                                        5.0,
                                        lsystem.name()))
            turt.process()
            if is_verbose:
                print(turt)
            start = time.time()
            turt.save_image(image_filename)
            duration_s = time.time() - start
            if is_verbose:
                print('Calculation time: {:.3f} s'.format(duration_s))

if __name__ == '__main__':
    main()
