img/Cantor_set_5_0_3.eps:	../systems/turtle/turtle_Cantor_set.txt ../systems/Cantor_set.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 5 --distance 3 --angle 0 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))


img/dragon_curve_10_8_90.eps:	../systems/turtle/turtle_dragon_curve.txt ../systems/dragon_curve.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 10 --distance 8 --angle 90 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))


img/fractal_binary_tree_0_10_90.eps:	../systems/turtle/turtle_fractal_binary_tree.txt ../systems/fractal_binary_tree.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 0 --distance 10 --angle 90 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/fractal_binary_tree_1_10_90.eps:	../systems/turtle/turtle_fractal_binary_tree.txt ../systems/fractal_binary_tree.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 1 --distance 10 --angle 90 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/fractal_binary_tree_2_10_90.eps:	../systems/turtle/turtle_fractal_binary_tree.txt ../systems/fractal_binary_tree.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 2 --distance 10 --angle 90 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/fractal_binary_tree_3_10_90.eps:	../systems/turtle/turtle_fractal_binary_tree.txt ../systems/fractal_binary_tree.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 3 --distance 10 --angle 90 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/fractal_binary_tree_4_10_90.eps:	../systems/turtle/turtle_fractal_binary_tree.txt ../systems/fractal_binary_tree.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 4 --distance 10 --angle 90 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/fractal_binary_tree_5_10_90.eps:	../systems/turtle/turtle_fractal_binary_tree.txt ../systems/fractal_binary_tree.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 5 --distance 10 --angle 90 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/fractal_binary_tree_6_10_90.eps:	../systems/turtle/turtle_fractal_binary_tree.txt ../systems/fractal_binary_tree.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 6 --distance 10 --angle 90 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/fractal_binary_tree_7_10_90.eps:	../systems/turtle/turtle_fractal_binary_tree.txt ../systems/fractal_binary_tree.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 7 --distance 10 --angle 90 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))


img/fractal_plant_6_3_65.eps:	../systems/turtle/turtle_fractal_plant.txt ../systems/fractal_plant.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 6 --distance 3 --angle 65 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))


img/Koch_curve_1_10_0.eps:	../systems/turtle/turtle_Koch_curve.txt ../systems/Koch_curve.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 1 --distance 10 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/Koch_curve_2_10_0.eps:	../systems/turtle/turtle_Koch_curve.txt ../systems/Koch_curve.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 2 --distance 10 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/Koch_curve_3_10_0.eps:	../systems/turtle/turtle_Koch_curve.txt ../systems/Koch_curve.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 3 --distance 10 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))


img/Koch_snowflake_1_10_0.eps:	../systems/turtle/turtle_Koch_snowflake.txt ../systems/Koch_snowflake.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 1 --distance 10 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/Koch_snowflake_2_10_0.eps:	../systems/turtle/turtle_Koch_snowflake.txt ../systems/Koch_snowflake.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 2 --distance 10 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/Koch_snowflake_3_10_0.eps:	../systems/turtle/turtle_Koch_snowflake.txt ../systems/Koch_snowflake.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 3 --distance 10 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/Koch_snowflake_4_10_0.eps:	../systems/turtle/turtle_Koch_snowflake.txt ../systems/Koch_snowflake.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 4 --distance 10 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/Koch_snowflake_5_10_0.eps:	../systems/turtle/turtle_Koch_snowflake.txt ../systems/Koch_snowflake.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 5 --distance 10 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))


img/Sierpinski_triangle_2_64_90.eps:	../systems/turtle/turtle_Sierpinski_triangle.txt ../systems/Sierpinski_triangle.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 2 --distance 64 --angle 90 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/Sierpinski_triangle_4_16_90.eps:	../systems/turtle/turtle_Sierpinski_triangle.txt ../systems/Sierpinski_triangle.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 4 --distance 16 --angle 90 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/Sierpinski_triangle_6_4_90.eps:	../systems/turtle/turtle_Sierpinski_triangle.txt ../systems/Sierpinski_triangle.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 6 --distance 4 --angle 90 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))


img/Sierpinski_arrowhead_curve_2_64_0.eps:	../systems/turtle/turtle_Sierpinski_arrowhead_curve.txt ../systems/Sierpinski_arrowhead_curve.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 2 --distance 64 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/Sierpinski_arrowhead_curve_4_16_0.eps:	../systems/turtle/turtle_Sierpinski_arrowhead_curve.txt ../systems/Sierpinski_arrowhead_curve.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 4 --distance 16 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/Sierpinski_arrowhead_curve_6_4_0.eps:	../systems/turtle/turtle_Sierpinski_arrowhead_curve.txt ../systems/Sierpinski_arrowhead_curve.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 6 --distance 4 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))

img/Sierpinski_arrowhead_curve_8_1_0.eps:	../systems/turtle/turtle_Sierpinski_arrowhead_curve.txt ../systems/Sierpinski_arrowhead_curve.txt
	$(PROG_L_SYSTEM) $(PROG_L_SYSTEMFLAGS) --nb-step 8 --distance 1 --image $@ --mapping $^ > $(patsubst img/%,log/%,$(patsubst %.eps,%_eps.log,$@))
