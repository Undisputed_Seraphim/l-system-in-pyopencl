#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
Process data produced by ../src/benchmarks.py.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 24, 2018
"""

import collections
import sys

import stats


K_NB_ITERATION = 0
K_IMPLEMENTATION = 1
K_IS_GPU = 2
K_NB_WORK_ITEM = 3

LENGTH = 0
NB_CORRECT = 1
AVG = 2
AVG_OPENCL = 3
MIN = 4
MAX = 5
MIN_OPENCL = 6
MAX_OPENCL = 7
CONFIDENCE = 8
CONFIDENCE_OPENCL = 9


def convert_item(item):
    assert isinstance(item, str), type(item)
    assert item != ''

    if 'LSystem' in item:  # implementation
        return item
    elif 'PU' in item:     # GPU or CPU
        return item
    elif item == '[]':     # empty list of times
        return []
    elif '[' in item:      # list of times
        return [convert_item(x) for x in item[1:-1].split(',')]
    elif '.' in item:      # float
        return float(item)
    else:                  # int
        return int(item)


def data_get_by_key(data, i_key, value, i_info, inverse=False):
    seq = []

    for key, infos in sorted(data.items()):
        if equal(key[i_key], value, inverse=inverse):
            seq.append(infos[i_info])

    return (seq[0] if len(seq) == 1
            else None)


def dict_filter(data, i, k, inverse=False):
    assert isinstance(data, dict), type(data)

    assert isinstance(i, int), type(i)
    assert i >= 0, i

    return {key: infos for key, infos in data.items()
            if (equal(key[i], k, inverse=inverse) if (i < len(key))
                else equal(infos[i - len(key)], k, inverse=inverse))}


def equal(a, b, inverse=False):
    return (not inverse and (a == b)) or (inverse and (a != b))


def filters(data, filts):
    assert isinstance(data, dict), type(data)
    assert isinstance(filts, collections.Iterable), type(filts)

    if filts:
        assert isinstance(filts[0], collections.Sequence), type(filts[0])
        assert 2 <= len(filts[0]) <= 3, filts[0]

        return filters(dict_filter(data, *filts[0]), filts[1:])
    else:
        return data


def load_raw_data(filename):
    """
    Read file at format produced by ../src/benchmarks.py.
0
2
LSystem
0
CPU
1
1
0.000013
0
10
0.000001
0
[0.000001, 0.000002, 0.000001, 0.000002, 0.000001, 0.000002, 0.000001, 0.000001, 0.000001, 0.000001]
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    """
    data = dict()

    with open(filename) as fin:
        line = fin.readline().rstrip()
        pieces = line.split('\t')
        nb_skip = int(pieces[15])
        nb_repetition = int(pieces[16])

        assert nb_skip >= 0
        assert nb_repetition >= 1

        for line in fin:
            line = line.rstrip()
            pieces = line.split('\t')
            nb_iteration, length, implementation, \
                device, gpu, nb_work_group, nb_work_item, \
                total_time, total_opencl, nb_correct, avg, avg_opencl, \
                times, times_opencl = (convert_item(item) for item in pieces)

            assert nb_iteration >= 0
            assert length >= 1
            assert 'LSystem' in implementation
            assert device in (0, 1)
            assert gpu in ('GPU', 'CPU')
            assert nb_work_group == 1
            assert nb_work_item >= 1
            assert total_time >= 0
            assert total_opencl >= 0
            assert 0 <= nb_correct <= nb_repetition

            assert avg >= 0
            if times:
                assert round(avg, 3) == round(stats.mean(times), 3)
            else:
                assert avg == 0

            assert avg_opencl >= 0
            if times_opencl:
                assert round(avg_opencl) == round(stats.mean(times_opencl))
            else:
                assert avg_opencl == 0

            assert isinstance(times, list)
            assert isinstance(times_opencl, list)

            if False:
                if nb_correct != nb_repetition:
                    print(line)

            if times:
                time_min = min(times)
                time_max = max(times)
            else:
                time_min, time_max = None, None

            if times_opencl:
                time_opencl_min = min(times_opencl)
                time_opencl_max = max(times_opencl)
            else:
                time_opencl_min, time_opencl_max = None, None

            geometric_avg = stats.geometric_mean(times)
            geometric_avg_opencl = stats.geometric_mean(times_opencl)

            key = (nb_iteration, implementation, (gpu == 'GPU'), nb_work_item)
            infos = (length,
                     nb_correct,
                     geometric_avg,
                     geometric_avg_opencl,
                     time_min, time_max,
                     time_opencl_min, time_opencl_max,
                     stats.confidence(times, avg=geometric_avg),
                     stats.confidence(times_opencl, avg=geometric_avg_opencl))

            assert key not in data

            data[key] = infos

    return data


def print_data(data, sort_index=None, reverse=False, header=None, filename=None):
    if filename is None:
        fout = sys.stdout
    else:
        fout = open(filename, 'w')

    if header is not None:
        assert isinstance(header, collections.Iterable), type(header)

        if isinstance(header, str):
            print(header, file=fout)
        else:
            print(*[to_string(item) for item in header], sep='\t', file=fout)

    seq = []
    for key, infos in sorted(data.items(), reverse=reverse):
        seq.append(key + ('|', ) + infos)

    sort_fct = (None if sort_index is None
                else lambda t: t[sort_index])

    for line in sorted(seq, key=sort_fct, reverse=reverse):
        print('\t'.join(to_string(item) for item in line), file=fout)

    if filename is not None:
        fout.close()


def to_string(item):
    if item is None:
        return ''
    elif isinstance(item, int):
        return str(item)
    elif isinstance(item, float):
        return '{:.6f}'.format(item)
    elif isinstance(item, list) or isinstance(item, tuple):
        return '\t'.join(to_string(x) for x in item)
    else:
        return str(item)
