# -*- coding: latin-1 -*-

"""
Simple function of speedup, efficiency and overhead
for parallel context.

:license: GPLv3 --- Copyright (C) 2018 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: August 24, 2018
"""

from __future__ import division
from __future__ import print_function


def efficiency(t_seq, t_par, p):
    assert isinstance(t_seq, int) or isinstance(t_seq, float), type(t_seq)
    assert isinstance(t_par, int) or isinstance(t_par, float), type(t_par)

    assert isinstance(p, int), type(p)
    assert p >= 1, p

    return speedup(t_seq, t_par) / p


def overhead(t_seq, t_par, p):
    assert isinstance(t_seq, int) or isinstance(t_seq, float), type(t_seq)
    assert isinstance(t_par, int) or isinstance(t_par, float), type(t_par)

    assert isinstance(p, int), type(p)
    assert p >= 1, p

    return t_par * p - t_seq


def speedup(t_seq, t_par):
    assert isinstance(t_seq, int) or isinstance(t_seq, float), type(t_seq)
    assert isinstance(t_par, int) or isinstance(t_par, float), type(t_par)

    return float(t_seq) / t_par
